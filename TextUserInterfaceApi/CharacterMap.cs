﻿namespace TextUserInterfaceApi;

public class CharacterMap
{
    public int Width
    {
        get => Characters.GetLength(1);
    }

    public int Height
    {
        get => Characters.GetLength(0);
    }

    public ColorChar[,] Characters { get; private set; }

    public CharacterMap(int width, int height)
    {
        Characters = new ColorChar[height, width];

        // Set default values.
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                Characters[y, x] = new ColorChar(' ', CharColor.Transparant, CharColor.Transparant);
            }
        }
    }


    /// <summary>
    /// Writes the characterMap on the console, then resets cursor position.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Interoperability", "CA1416:Validate platform compatibility", Justification = "I know that windows only stuff is being used here.")]
    public void WriteToConsole()
    {
        // Print the last character in the place of the second to last character.
        if (Console.BufferHeight >= Console.WindowHeight && Console.BufferWidth >= Console.WindowWidth)
        {
            List<ColorString> strings = CompileStrings();

            WriteStrings(strings);
            HandleLastCharacters();

            try
            {
                // Reset cursor and window position.
                Console.SetCursorPosition(0, 0);
                Console.SetWindowPosition(0, 0);
            }
            catch (IOException) { }

            // making sure space outside buffer area is black after a resize
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Interoperability", "CA1416:Validate platform compatibility", Justification = "I know that windows only stuff is being used here.")]
    private void HandleLastCharacters()
    {
        try
        {
            if (Width - 2 < Console.BufferWidth && Width - 2 > 0)
            {
                Console.CursorLeft = Width - 2;
                Console.ForegroundColor = ColorChar.ParseConsoleColor(Characters[Height - 1, Width - 1].FGColor);
                Console.BackgroundColor = ColorChar.ParseConsoleColor(Characters[Height - 1, Width - 1].BGColor);
                Console.Write(Characters[Height - 1, Width - 1].Character);

                // Move the buffer area 1 char to the left and replace the
                // Character with the last character of the map to avoid cursor going to the next line.
                Console.MoveBufferArea(
                    sourceLeft: Width - 2,
                    sourceTop: Height - 1,
                    sourceWidth: 1,
                    sourceHeight: 1,
                    targetLeft: Width - 1,
                    targetTop: Height - 1,
                    sourceChar: Characters[Height - 1, Width - 2].Character.Value,
                    sourceForeColor: ColorChar.ParseConsoleColor(Characters[Height - 1, Width - 2].FGColor),
                    sourceBackColor: ColorChar.ParseConsoleColor(Characters[Height - 1, Width - 2].BGColor)
                );
            }
        }
        catch (ArgumentOutOfRangeException)
        {
            // BufferArea could not be moved because the move happened while the console window was being resized.
            // This error can be safely ignored because it does not impact te application.
        }
    }

    /// <summary>
    /// Writes strings to the console.
    /// </summary>
    /// <param name="strings">List of strings to write.</param>
    private static void WriteStrings(List<ColorString> strings)
    {
        strings.ForEach(str =>
        {
            Console.ForegroundColor = ColorChar.ParseConsoleColor(str.FGColor);
            Console.BackgroundColor = ColorChar.ParseConsoleColor(str.BGColor);
            Console.Write(str.Str);
        });
    }

    /// <summary>
    /// Pastes a charactermap into the current character map.
    /// </summary>
    /// <param name="characterMap">charmap to paste</param>
    /// <param name="targetX">X position to paste the content onto.</param>
    /// <param name="targetY">Y position to paste the content onto.</param>
    public void PasteMap(CharacterMap characterMap, int targetX, int targetY)
    {
        for (int y = 0; y < Height; y++)
        {
            for (int x = 0; x < Width; x++)
            {
                if (CheckCoordInsideDimensions(x, targetX, y, targetY))
                {
                    if (x < characterMap.Width && y < characterMap.Height)
                    {
                        char? character = characterMap.Characters[y, x].Character;
                        CharColor fgColor = characterMap.Characters[y, x].FGColor;
                        CharColor bgColor = characterMap.Characters[y, x].BGColor;

                        // if character is null, use character from old charmap state instead
                        if (character == null)
                        {
                            character = Characters[y + targetY, x + targetX].Character;
                        }

                        // if character has transparent background color, use background color from old charmap state instead
                        if (bgColor == CharColor.Transparant)
                        {
                            bgColor = Characters[y + targetY, x + targetX].BGColor;
                        }

                        // if character has transparent foreground color, use foreground colorr from old charmap state instead
                        if (fgColor == CharColor.Transparant)
                        {
                            fgColor = Characters[y + targetY, x + targetX].FGColor;
                        }

                        Characters[y + targetY, x + targetX].Character = character;
                        Characters[y + targetY, x + targetX].FGColor = fgColor;
                        Characters[y + targetY, x + targetX].BGColor = bgColor;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Compiles the charMap into a list of strings.
    /// </summary>
    /// <returns>List of colorStrings.</returns>
    private List<ColorString> CompileStrings()
    {
        List<ColorString> strings = new();
        StringBuilder builder = new();

        CharColor fgColor = Characters[0, 0].FGColor;
        CharColor bgColor = Characters[0, 0].BGColor;

        // Compile the strings.
        for (int y = 0; y < Height; y++)
        {
            for (int x = 0; x < Width; x++)
            {
                bool breakString = false;

                if (Characters[y, x].Character == null)
                {
                    Characters[y, x].Character = ' ';
                }

                // Check if coordinates are not on last two.
                if (!(y == Height - 1 && x >= Width - 3))
                {
                    // Decide if color config is still the same.
                    if (Characters[y, x].FGColor == fgColor && Characters[y, x].BGColor == bgColor)
                    {
                        builder.Append(Characters[y, x].Character);
                    }
                    else
                    {
                        breakString = true;
                    }
                }
                else
                {
                    breakString = true;
                }

                if (breakString)
                {
                    strings.Add(new ColorString(builder.ToString(), fgColor, bgColor));
                    builder.Clear();

                    fgColor = Characters[y, x].FGColor;
                    bgColor = Characters[y, x].BGColor;
                    builder.Append(Characters[y, x].Character);
                }
            }
        }

        return strings;
    }

    private bool CheckCoordInsideDimensions(int x, int targetX, int y, int targetY)
    {
        return x + targetX >= 0 && y + targetY >= 0 && x + targetX < Width && y + targetY < Height;
    }
}
