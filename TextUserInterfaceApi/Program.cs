﻿using TextUserInterfaceApi.Ui;

TextUserInterface textUserInterface = new() { Title = "Tui demo - made by Dennis Angevaare (2021)" };

textUserInterface.Run(new DemoUserInterface());