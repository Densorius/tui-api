﻿namespace TextUserInterfaceApi.Utils;

/// <summary>
/// This is a helper class to abstract away the console cursor. This can be used to save the position so it can be restored later.
/// </summary>
public class ConsoleCursorHelper
{
    public bool CursorVisible
    {
        get => _cursorVisible;
        set
        {
            _cursorVisible = value;
            Console.CursorVisible = value;
        }
    }

    public int Top
    {
        get => _top;
        set
        {
            _top = value;
            Console.CursorTop = value;
        }
    }

    public int Left
    {
        get => _left;
        set
        {
            _left = value;
            Console.CursorLeft = value;
        }
    }

    public int SavedTop { get; private set; }
    public int SavedLeft { get; private set; }

    private int _top;
    private int _left;
    private bool _cursorVisible;

    public ConsoleCursorHelper()
    {
        Top = 0;
        Left = 0;
        CursorVisible = true;

        SaveCursorPosition();
    }

    public ConsoleCursorHelper(int top, int left) : base()
    {
        SetCursorPostion(top, left);
    }

    public void SetCursorPostion(int top, int left)
    {
        Top = top;
        Left = left;
    }

    /// <summary>
    /// Writes the saved cursor position from this class into the Console's cursor position.
    /// </summary>
    public void WriteCursorPositionToConsole()
    {
        Console.SetCursorPosition(Left, Top);
    }

    public (int top, int left) GetCursorPosition()
    {
        return (Top, Left);
    }

    /// <summary>
    /// Gets the saved cursor position. This position is seperate from the actual cursor position.
    /// </summary>
    /// <returns>Tuple containing the top and left value of the console cursor position.</returns>
    public (int top, int left) GetSavedCursorPosition()
    {
        return (SavedTop, SavedLeft);
    }

    /// <summary>
    /// Saves the current cursor position.
    /// </summary>
    public void SaveCursorPosition()
    {
        SavedTop = Top;
        SavedLeft = Left;
    }

    /// <summary>
    /// Restores the cursor position. Note that this also effects the console's cursor.
    /// </summary>
    public void RestoreCorsorPosition()
    {
        Top = SavedTop;
        Left = SavedLeft;
    }
}
