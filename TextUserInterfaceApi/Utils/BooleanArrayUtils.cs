﻿namespace TextUserInterfaceApi.Utils;

public class BooleanArrayUtils
{
    /// <summary>
    /// Converts a given byte number into a boolean array with a given amount of booleans.
    /// </summary>
    /// <param name="number">Number to convert.</param>
    /// <returns>Boolean array.</returns>
    public static bool[] ByteToBooleanArray(byte number, byte amountOfBooleans = 8)
    {
        bool[] booleans = new bool[amountOfBooleans];

        for (int i = 0; i < amountOfBooleans; i++)
        {
            booleans[i] = (number >> i & 1) == 1;
        }

        return booleans;
    }

    /// <summary>
    /// Converts a given boolean array into a byte. Note that the maximum amount of booleans is 4.
    /// </summary>
    /// <param name="booleanArray">Boolean array to convert.</param>
    /// <returns>Byte with their bits corrosponding with the values in the boolean array</returns>
    public static byte BooleanArrayToByte(bool[] booleanArray)
    {
        if (booleanArray.Length > 4)
        {
            throw new ArgumentException("The amound of booleans in the array should be less than or equal to 4");
        }

        int number = 0b0000;
        int position = 0;

        for (int i = booleanArray.Length - 1; i >= 0; i--)
        {
            if (booleanArray[i])
                number += 1 << position;

            position++;
        }

        return (byte)number;
    }
}
