﻿namespace TextUserInterfaceApi.Utils;

class StringUtils
{
    /// <summary>
    /// Adds spaces to a given string.
    /// </summary>
    /// <param name="str">String to modify.</param>
    /// <param name="spaces">Amound of spaces.</param>
    /// <param name="end">If spaces should on end of string</param>
    /// <returns>string with spaces added</returns>
    public static string SpacedString(string str, int spaces, bool end = false)
    {
        StringBuilder builder = new();

        if (end)
        {
            builder.Append(str);
        }

        for (int i = 0; i < spaces; i++)
        {
            builder.Append(' ');
        }

        return end ? builder.ToString() : builder.Append(str).ToString();
    }

    /// <summary>
    /// Aligns a string withing a larger string using spaces.
    /// </summary>
    /// <param name="str">The string to align</param>
    /// <param name="width">The width the string needs to ocupy.</param>
    /// <param name="textAlignment">The alignment the string should use.</param>
    /// <returns>A aligned string with spaces.</returns>
    public static string AlignString(string str, int width, TextAlignment textAlignment)
    {
        StringBuilder stringBuilder = new();

        switch (textAlignment)
        {
            case TextAlignment.LEFT:
                LeftAlignString(str, width, stringBuilder);
                break;

            case TextAlignment.CENTER:
                CenterAlignString(str, width, stringBuilder);
                break;

            case TextAlignment.RIGHT:
                RigtAlignString(str, width, stringBuilder);
                break;
        }

        return stringBuilder.ToString();
    }

    /// <summary>
    /// Left aligns a string using a certain width.
    /// </summary>
    /// <param name="str">String to align</param>
    /// <param name="width">Desired width</param>
    /// <param name="stringBuilder">StringBuilder to use</param>
    private static void LeftAlignString(string str, int width, StringBuilder stringBuilder)
    {
        stringBuilder.Append(str);
        AppendSpacesToStringBuilder(width - str.Length, stringBuilder);
    }

    /// <summary>
    /// Right aligns a string using a certain width.
    /// </summary>
    /// <param name="str">String to align</param>
    /// <param name="width">Desired width</param>
    /// <param name="stringBuilder">StringBuilder to use</param>
    private static void RigtAlignString(string str, int width, StringBuilder stringBuilder)
    {
        AppendSpacesToStringBuilder(width - str.Length, stringBuilder);
        stringBuilder.Append(str);
    }

    /// <summary>
    /// Center aligns a string using a certain width.
    /// </summary>
    /// <param name="str">String to align</param>
    /// <param name="width">Desired width</param>
    /// <param name="stringBuilder">StringBuilder to use</param>
    private static void CenterAlignString(string str, int width, StringBuilder stringBuilder)
    {
        int spacesStart = width / 2 - str.Length / 2;
        int spacesEnd = width - str.Length - spacesStart;

        AppendSpacesToStringBuilder(spacesStart, stringBuilder);
        stringBuilder.Append(str);
        AppendSpacesToStringBuilder(spacesEnd, stringBuilder);
    }

    /// <summary>
    /// Appends a series of spaces on a stringBuilder.
    /// </summary>
    /// <param name="amout">Amount of spaces to append.</param>
    /// <param name="stringBuilder">The stringBuilder to use.</param>
    public static void AppendSpacesToStringBuilder(int amout, StringBuilder stringBuilder)
    {
        for (int i = 0; i < amout; i++)
        {
            stringBuilder.Append(' ');
        }
    }

    /// <summary>
    /// Gets the length of the longest string.
    /// </summary>
    /// <param name="fullText">array of strings to check on</param>
    /// <returns>Length of longest string.</returns>
    public static int GetLongestStringLengthInList(List<string> fullText)
    {
        string longestString = fullText.OrderByDescending(str => str.Length).FirstOrDefault() ?? string.Empty;
        return longestString.Length;
    }
}
