﻿namespace TextUserInterfaceApi.Utils;

/// <summary>
/// Contains extention methods for normal arrays.
/// </summary>
public static class ArrayExtentions
{
    public delegate void action<T>(T item);

    /// <summary>
    /// Like the ForEach for a list, this runs a for loop on the array and for each item in that array calls the given delegate.
    /// </summary>
    /// <typeparam name="T">Type of items in array.</typeparam>
    /// <param name="array">The array itself.</param>
    /// <param name="action">Delegate to run for each item in the array.</param>
    public static void ForEach<T>(this T[] array, action<T> action)
    {
        foreach (var item in array)
        {
            action(item);
        }
    }
}
