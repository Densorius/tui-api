﻿namespace TextUserInterfaceApi.Utils;

/// <summary>
/// Represents a List with selectable items.
/// </summary>
public class SelectableList<T>
{
    public int SelectedIndex;

    /// <summary>
    /// Get or set value of the index in the underlying list
    /// </summary>
    /// <param name="index">Index in the list.</param>
    /// <returns>Value at index</returns>
    public T this[int index]
    {
        get => Items[index];
        set => Items[index] = value;
    }

    public T Selected => Items[SelectedIndex];

    /// <summary>
    /// Gets the amount of items in the list.
    /// </summary>
    public int Count
    {
        get => Items.Count;
    }

    /// <summary>
    /// Gets or sets the items list. When set the SelectedIndex is automatically set to the first non null value index.
    /// </summary>
    public List<T> Items
    {
        get => _items;
        set
        {
            _items = value;
            SelectedIndex = Items.IndexOf(Items.Where(item => item != null).FirstOrDefault());
        }
    }

    private List<T> _items;

    private const string EMPTY_LIST_ERROR_MESSAGE = "List only contains null values and thus no item can be selected\n" +
                "Please add at least one non null item";

    public SelectableList()
    {
        SelectedIndex = 0;

        Items = new List<T>();
    }

    public SelectableList(List<T> items)
    {
        Items = items;
        SetIndexToFirst();
    }

    /// <summary>
    /// Keeps increasing the index until it points to a non null item.
    /// </summary>
    public void IncreaseIndex()
    {
        if (Items.Where(item => item != null).Any())
        {
            do
            {
                SelectedIndex++;

                if (SelectedIndex > Count - 1)
                {
                    SelectedIndex = 0;
                }
            }
            while (Items[SelectedIndex] == null);
        }
        else
        {
            throw new ArgumentException(EMPTY_LIST_ERROR_MESSAGE);
        }
        //IncreaseOrDecreaseIndex(false);
    }

    /// <summary>
    /// Keeps decreasing the index until it points to a non null item.
    /// </summary>
    public void DecreaseIndex()
    {
        if (Items.Where(item => item != null).Any())
        {
            do
            {
                SelectedIndex--;

                if (SelectedIndex < 0)
                {
                    SelectedIndex = Items.Count - 1;
                }
            }
            while (Items[SelectedIndex] == null);
        }
        else
        {
            throw new ArgumentException(EMPTY_LIST_ERROR_MESSAGE);
        }
        //IncreaseOrDecreaseIndex(true);
    }

    /// <summary>
    /// Sets the index to the first non null item
    /// </summary>
    public void SetIndexToFirst()
    {
        SelectedIndex = Items.IndexOf(Items.Where(item => item != null).FirstOrDefault());
    }

    /// <summary>
    /// Sets the index to the last non null item
    /// </summary>
    public void SetIndexToLast()
    {
        SelectedIndex = Items.IndexOf(Items.Where(item => item != null).LastOrDefault());
    }

    /// <summary>
    /// Adds the given item to the list.
    /// </summary>
    /// <param name="item">Item to add.</param>
    public void Add(T item)
    {
        Items.Add(item);
    }
}
