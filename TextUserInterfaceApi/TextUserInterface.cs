﻿using System.Diagnostics;
using TextUserInterfaceApi.Interface;
using TextUserInterfaceApi.Utils;

namespace TextUserInterfaceApi;

public class TextUserInterface
{
    public int Width { get; set; }
    public int Height { get; set; }

    public bool UpdateOnKeyPress { get; set; }

    public ConsoleCursorHelper ConsoleCursorHelper { get; private set; }

    /// <summary>
    /// Changes title of window
    /// </summary>
    public string Title
    {
        get => _title;
        set
        {
            _title = value;
            Console.Title = value;
        }
    }

    /// <summary>
    /// The root element of the user interface. 
    /// When changing the container for another one, that container will be resized to fit the console
    /// </summary>
    public Container RootContainer
    {
        get => _rootContainer;
        set
        {
            _rootContainer = value;
            _rootContainer.Width = Console.WindowWidth;
            _rootContainer.Height = Console.WindowHeight;
        }
    }

    /// <summary>
    /// Fired when a resize was detected.
    /// </summary>
    public event EventHandler Resize;

    private string _title;
    private bool _continue;
    private bool _canUpdate;

    private Container _rootContainer;
    private CharacterMap _characterMap;

    private readonly int _oldBufferHeight;
    private readonly ConsoleColor _oldFGColor;
    private readonly ConsoleColor _oldBGColor;

    public TextUserInterface()
    {
        try
        {
            _oldBufferHeight = Console.BufferHeight;

            _oldFGColor = Console.ForegroundColor;
            _oldBGColor = Console.BackgroundColor;

            Width = Console.BufferWidth;
            Height = Console.WindowHeight;

            ConsoleCursorHelper = new ConsoleCursorHelper
            {
                CursorVisible = false
            };

            UpdateOnKeyPress = true;

            _characterMap = new CharacterMap(Width, Height);
            _canUpdate = true;
            _continue = true;
        }
        catch (IOException)
        {
            Console.WriteLine("Unsupported command line...");
            Environment.Exit(0);
        }
    }

    public void Run(UserInterface userInterface)
    {
        InitializeUserInterface(userInterface);
        Launch();
    }

    public void SwitchInterface(UserInterface userInterface)
    {
        InitializeUserInterface(userInterface);
        Update();
    }

    /// <summary>
    /// Launches the user interface.
    /// </summary>
    public void Launch()
    {
        if (_rootContainer == null)
        {
            throw new InvalidOperationException("There is no root container assigned.");
        }

        try
        {
#pragma warning disable CA1416 // Validate platform compatibility                
            Console.BufferHeight = Console.WindowHeight;
#pragma warning restore CA1416 // Validate platform compatibility
        }
        catch (ArgumentOutOfRangeException)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;

            Console.WriteLine("Could not set height of buffer.");
            Console.WriteLine("TUI can still run however");
        }

        Console.OutputEncoding = Encoding.UTF8;

        // run update for the first time.
        Update();

        // activate main update loop
        while (_continue)
        {
            HandleKeypress();
            HandleResize();

            // be friendly to the cpu.
            // This can cause errors with resizing the window when a resize occures when the thread is interrupted.
            // These errors need to be caught but otherwise can be ignored.
            Thread.Sleep(1);
        }
    }

    /// <summary>
    /// Updates the user interface.
    /// </summary>
    public void Update()
    {
        if (_continue && _rootContainer != null && _canUpdate)
        {
            _canUpdate = false;

            _rootContainer.Update(_characterMap);

            ConsoleCursorHelper.SaveCursorPosition();
            _characterMap.WriteToConsole();
            ConsoleCursorHelper.RestoreCorsorPosition();

            _canUpdate = true;
        }
    }

    /// <summary>
    /// Closes the user interface and resets any modified value.
    /// </summary>
    /// <param name="clearScreen">Whether to clear the screen or not (default is true).</param>
    public void Close(bool clearScreen = true)
    {
        _continue = false;

        if (Console.BufferHeight < _oldBufferHeight)
        {
#pragma warning disable CA1416 // Validate platform compatibility
            Console.BufferHeight = _oldBufferHeight;
#pragma warning restore CA1416 // Validate platform compatibility
        }

        Console.ResetColor();
        Console.CursorVisible = true;

        if (clearScreen)
        {
            Console.Clear();
        }
    }

    /// <summary>
    /// Gets a element by it's id.
    /// </summary>
    /// <typeparam name="T">Type of expected element.</typeparam>
    /// <param name="id">id of expected element.</param>
    /// <returns>Element with id.</returns>
    public T GetElementById<T>(string id) where T : Element
    {
        T element = GetElementById<T>(id, _rootContainer);

        return element;
    }

    private void InitializeUserInterface(UserInterface userInterface)
    {
        ConsoleCursorHelper.CursorVisible = false;
        userInterface.InitializeUserInterface(this);
    }

    /// <summary>
    /// Gets a element by it's id.
    /// </summary>
    /// <typeparam name="T">Type of expected element</typeparam>
    /// <param name="id">id of expected element</param>
    /// <param name="parent">parent of the element (only used in this class)</param>
    /// <returns>Element with id</returns>
    private T GetElementById<T>(string id, IParent parent) where T : Element
    {
        bool elementFound = false;

        if (parent != null)
        {
            foreach (Element child in parent.ChildElements)
            {
                if (child != null && child.Id != null && child.Id.Equals(id))
                {
                    if (!typeof(T).Equals(child.GetType()))
                    {
                        throw new ArgumentException("Element is not of specified type");
                    }
                    else
                    {
                        elementFound = true;
                        return child as T;
                    }
                }

                if (!elementFound && child is IParent)
                {
                    return GetElementById<T>(id, child as IParent);
                }
            }
        }

        Debug.WriteLine("Element is null");

        return null;
    }

    /// <summary>
    /// Calls the OnKeyPressed method of all elements in the user interface.
    /// </summary>
    /// <param name="element">element to call it's OnKeyPressed method</param>
    /// <param name="keyInfo">ConsoleKey object to pass to the OnKeyPressed method</param>
    private void KeyPressTriggered(Element element, ConsoleKeyInfo keyInfo)
    {
        element.KeyPressTriggered(keyInfo);

        List<Element> elementsToUpdate = new();

        if (element is IParent parentElement)
        {
            Element elementWithHighesFocusLevel = GetElementWIthHighestFocusLevel(parentElement.ChildElements);

            if (elementWithHighesFocusLevel != null)
            {
                elementsToUpdate.Add(elementWithHighesFocusLevel);
            }
            else
            {
                foreach (Element child in parentElement.ChildElements)
                {
                    elementsToUpdate.Add(child);
                }
            }
        }

        elementsToUpdate.ForEach(elementToUpdate => KeyPressTriggered(elementToUpdate, keyInfo));
    }

    private static Element GetElementWIthHighestFocusLevel(List<Element> elements)
    {
        List<IFocusable> focusables = new();

        elements.ForEach(child =>
        {
            if (child is IFocusable focusable)
            {
                if (focusable.Focus)
                {
                    focusables.Add(focusable);
                }
            }
        });

        if (focusables.Count > 0)
        {
            return focusables.OrderByDescending(focusable => focusable.FocusLevel).FirstOrDefault() as Element;
        }

        return null;
    }

    /// <summary>
    /// Handles the keypresses to the console window. Notifies all elements if there was a keypress.
    /// </summary>
    private void HandleKeypress()
    {
        if (Console.KeyAvailable && _continue)
        {
            KeyPressTriggered(RootContainer, Console.ReadKey(true));

            if (UpdateOnKeyPress)
            {
                Update();
            }
        }
    }

    /// <summary>
    /// Handle the resize of the Console window.
    /// </summary>
    private void HandleResize()
    {
        // Detect if console window resize has occured
        if (Console.WindowHeight != Height || Console.WindowWidth != Width)
        {
            if (Console.BufferHeight >= Console.WindowHeight &&
                Console.BufferWidth >= Console.WindowWidth &&
                Console.WindowHeight > 0 &&
                Console.WindowWidth > 15)
            {
                try
                {
#pragma warning disable CA1416 // Validate platform compatibility
                    Console.SetWindowPosition(0, 0);

                    Width = Console.BufferWidth;
                    Height = Console.WindowHeight;

                    // update bufferHeight.
                    Console.BufferHeight = Height;
                    Console.Clear(); // reduces chance of artefacts outside of the bufferarea for default windows console.
#pragma warning restore CA1416

                    if (_rootContainer != null)
                    {
                        _rootContainer.Width = Width;
                        _rootContainer.Height = Height;

                        _rootContainer.ResetCharacterMap(Width, Height);
                    }

                    _characterMap = new CharacterMap(Width, Height);

                    Console.CursorVisible = ConsoleCursorHelper.CursorVisible; // restore visibility of cursor after update.

                    Resize?.Invoke(this, new EventArgs());

                    Update();
                }
                catch (IOException)
                {
                    // could not set the position of the window because of window resize.
                    // This error is ignored because it does not impact the application.
                }
                catch (ArgumentOutOfRangeException)
                {
                    // Heigth of the buffer could not be updated because the window was still too big.
                    // This error is ignored because it does not impact te application.
                }
            }
            else
            {
                Console.ForegroundColor = _oldFGColor;
                Console.BackgroundColor = _oldBGColor;
                Console.WriteLine("\nUnsupported\nwindow size...\n");
            }
        }
    }
}
