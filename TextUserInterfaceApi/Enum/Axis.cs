﻿namespace TextUserInterfaceApi.Enum;

public enum Axis
{
    X,
    Y
}
