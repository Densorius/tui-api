﻿namespace TextUserInterfaceApi.Enum;

/// <summary>
/// Almost the same as ConsoleColor but has a extra value called `Transparant`
/// </summary>
public enum CharColor
{
    Black,
    DarkBlue,
    DarkGreen,
    DarkCyan,
    DarkRed,
    DarkMagenta,
    DarkYellow,
    Gray,
    DarkGray,
    Blue,
    Green,
    Cyan,
    Red,
    Magenta,
    Yellow,
    White,
    Transparant // the only new value
}
