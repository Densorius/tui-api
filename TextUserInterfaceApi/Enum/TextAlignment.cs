﻿namespace TextUserInterfaceApi.Enum;

public enum TextAlignment
{
    LEFT,
    CENTER,
    RIGHT
}
