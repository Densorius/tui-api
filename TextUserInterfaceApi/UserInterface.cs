﻿namespace TextUserInterfaceApi;

public abstract class UserInterface
{
    public CharColor FGColor { get; set; }
    public CharColor BGColor { get; set; }
    public TextUserInterface TextUserInterface { get; set; }
    public int Width => TextUserInterface.Width;
    public int Height => TextUserInterface.Height;

    public event EventHandler Initialized;

    public Container RootContainer { get; private set; }

    public UserInterface(CharColor fgColor = CharColor.Black, CharColor bgColor = CharColor.DarkBlue)
    {
        FGColor = fgColor;
        BGColor = bgColor;
    }

    public void InitializeUserInterface(TextUserInterface textUserInterface)
    {
        TextUserInterface = textUserInterface;

        RootContainer = new Container(TextUserInterface.Width, TextUserInterface.Height, FGColor, BGColor);
        TextUserInterface.RootContainer = RootContainer;

        InitializeComponents();
        Initialized?.Invoke(this, new EventArgs());
    }

    /// <summary>
    /// This calls the TextUserInterface close method which resets any modified console value.
    /// </summary>
    /// <param name="clearScreen">Whether to clear the screen or not (default is true).</param>
    public void Close(bool clearScreen = true)
    {
        if (TextUserInterface != null)
            TextUserInterface.Close(clearScreen);
    }

    protected abstract void InitializeComponents();
}
