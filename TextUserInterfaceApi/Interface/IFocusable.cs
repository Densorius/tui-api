﻿namespace TextUserInterfaceApi.Interface;

public interface IFocusable
{
    bool Focus { get; set; }
    int FocusLevel { get; }
}