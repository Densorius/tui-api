﻿namespace TextUserInterfaceApi.Interface;

public interface IParent : ICaracterMap
{
    List<Element> ChildElements { get; set; }

    /// <summary>
    /// Adds the provided element as a child of the current element.
    /// </summary>
    /// <param name="element">element to add</param>
    void AddElement(Element element);

    /// <summary>
    /// Adds the provided elements as children of the current element.
    /// </summary>
    /// <param name="elements">elements to add</param>
    void AddMultipleElements(params Element[] elements);
}
