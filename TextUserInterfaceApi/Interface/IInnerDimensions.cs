﻿namespace TextUserInterfaceApi.Interface;

interface IInnerDimensions
{
    int InnerWidth { get; }
    int InnerHeight { get; }
}