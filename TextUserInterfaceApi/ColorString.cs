﻿using TextUserInterfaceApi.Enum;

namespace TextUserInterfaceApi;

class ColorString
{
    public CharColor FGColor { get; set; }
    public CharColor BGColor { get; set; }
    public string Str { get; set; }

    /// <summary>
    /// Creates a colorstring with text and colors.
    /// </summary>
    /// <param name="str">Text of the string.</param>
    /// <param name="fgColor">Foreground color of the string.</param>
    /// <param name="bgColor">Background color of the string.</param>
    public ColorString(string str, CharColor fgColor, CharColor bgColor)
    {
        Str = str;
        FGColor = fgColor;
        BGColor = bgColor;
    }

    /// <summary>
    /// Creates a empty color string with black as default foreground color and gray for default background color
    /// </summary>
    public ColorString()
	{
        Str = string.Empty;
        FGColor = CharColor.Black;
        BGColor = CharColor.Gray;
	}
}
