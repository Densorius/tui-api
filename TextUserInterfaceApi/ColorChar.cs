﻿using TextUserInterfaceApi.Enum;

namespace TextUserInterfaceApi;

public class ColorChar
{
    public CharColor FGColor { get; set; }
    public CharColor BGColor { get; set; }
    public char? Character { get; set; }

    public ColorChar(char? character, CharColor fgColor, CharColor bgColor)
    {
        Character = character;
        FGColor = fgColor;
        BGColor = bgColor;
    }

    /// <summary>
    /// Converts the charcolor to consoleColor. Transparent color will be converted to black
    /// </summary>
    /// <param name="charColor">color to parse</param>
    /// <returns>ConsoleColor equivalent of charColor</returns>
    public static ConsoleColor ParseConsoleColor(CharColor charColor)
    {
        if (charColor == CharColor.Transparant)
        {
            return ConsoleColor.Black;
        }
        else
        {
            return (ConsoleColor)charColor;
        }
    }
}