﻿namespace TextUserInterfaceApi.Elements;

/// <summary>
/// Represents a container on which elements are placed by it's constructor.
/// </summary>
public class UnmutableContainer : Element
{
    private List<Element> _childElements;
    private CharacterMap _characterMap;

    public override void Update(CharacterMap characterMap)
    {
        throw new NotImplementedException();
    }
}
