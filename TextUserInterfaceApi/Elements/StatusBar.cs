﻿namespace TextUserInterfaceApi.Elements
{
    /// <summary>
    /// Reprisents a statusbar to be displayed at the bottom of a parent element such as a container.
    /// </summary>
    public class StatusBar : Element
    {
        /// <summary>
        /// Gets or sets the divider used to divide the different statusItems.
        /// </summary>
        public char Divider { get; set; }

		public List<Element> ChildElements;

        public CharacterMap CharacterMap;

		private readonly List<StatusItem> _statusItems;

        /// <summary>
        /// Initializes a new statusbar with some optional colors.
        /// </summary>
        /// <param name="fgColor">Foreground color of the statusbar.</param>
        /// <param name="bgColor">Background color of the statusbar.</param>
        public StatusBar(CharColor fgColor = CharColor.Black, CharColor bgColor = CharColor.Gray)
        {
            Height = 1;
            Divider = '│';
            FGColor = fgColor;
            BGColor = bgColor;

            _statusItems = new List<StatusItem>();
        }

        /// <summary>
        /// Adds a statusItem to the statusBar.
        /// </summary>
        /// <param name="statusItem">Statusitem to add.</param>
        public void AddStatusItem(StatusItem statusItem)
        {
            _statusItems.Add(statusItem);
        }

        /// <summary>
        /// Adds a variable number of statusItems to the statusBar.
        /// </summary>
        /// <param name="statusItems">StatusItem to add.</param>
        public void AddMultipleStatusItems(params StatusItem[] statusItems)
        {
            foreach (StatusItem statusItem in statusItems)
            {
                AddStatusItem(statusItem);
            }
        }

        public override void Update(CharacterMap characterMap)
        {
            Width = characterMap.Width;
            X = 0;
            Y = characterMap.Height - 1;

            string fullStatusText = GetFullStatusText();

            for (int x = 0; x < characterMap.Width; x++)
            {
                if (x < fullStatusText.Length)
                {
                    characterMap.Characters[Y, x] = new ColorChar(fullStatusText[x], FGColor, BGColor);
                }
                else
                {
                    characterMap.Characters[Y, x] = new ColorChar(' ', FGColor, BGColor);
                }
            }
        }

        /// <summary>
        /// Gets the full status text by adding al status items together and adding dividers to them.
        /// </summary>
        /// <returns>Full status text.</returns>
        private string GetFullStatusText()
        {
            StringBuilder builder = new();

            for (int i = 0; i < _statusItems.Count; i++)
            {
                string statusItemWithMargin = _statusItems[i].TextIncludingMargin;

                builder.Append(statusItemWithMargin);

                if (i < _statusItems.Count - 1)
                {
                    builder.Append(Divider);
                }
            }

            return builder.ToString();
        }
    }
}
