﻿namespace TextUserInterfaceApi.Elements;

/// <summary>
/// Represents a generic user interface element.
/// </summary>
public abstract class Element
{
    /// <summary>
    /// Gets or sets the X position.
    /// </summary>
    public virtual int X { get; set; }

    /// <summary>
    /// Gets or sets the Y position.
    /// </summary>
    public virtual int Y { get; set; }

    /// <summary>
    /// Gets or sets the Id.
    /// </summary>
    public string Id { get; set; }

    /// <summary>
    /// Gets or sets the Width.
    /// </summary>
    public virtual int Width { get; set; }

    /// <summary>
    /// Gets or sets the Height.
    /// </summary>
    public virtual int Height { get; set; }

    /// <summary>
    /// Gets or sets the foreground color.
    /// </summary>
    public CharColor FGColor { get; set; }

    /// <summary>
    /// Gets or sets the background color.
    /// </summary>
    public CharColor BGColor { get; set; }

    /// <summary>
    /// Gets or sets wheter the element is hidden or not.
    /// </summary>
    public virtual bool Hidden { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public Element Parent { get; set; }

    public Element()
    {
        Hidden = false;
        X = 0;
        Y = 0;
    }

    /// <summary>
    /// Sets both foreground and background color.
    /// </summary>
    /// <param name="fgColor">New foreground color.</param>
    /// <param name="bgColor">New background color.</param>
    public virtual void SetColor(CharColor fgColor, CharColor bgColor)
    {
        FGColor = fgColor;
        BGColor = bgColor;
    }

    /// <summary>
    /// Sets both width and height of the element.
    /// </summary>
    /// <param name="width">New width.</param>
    /// <param name="height">New height.</param>
    public virtual void SetSize(int width, int height)
    {
        Width = width;
        Height = height;
    }

    /// <summary>
    /// Called when a keypress was registered in the Console. This method is only meant for elements to use.
    /// </summary>
    /// <param name="keyInfo">KeyInfo of the key that was pressed.</param>
    public virtual void KeyPressTriggered(ConsoleKeyInfo keyInfo) { }

    /// <summary>
    /// Ran after being added to the userinterface.
    /// </summary>
    public virtual void AddedToInterface() { }

    /// <summary>
    /// Updates the element and modifies a given charactermap.
    /// </summary>
    /// <param name="characterMap">Character map to modify.</param>
    public abstract void Update(CharacterMap characterMap);

    /// <summary>
    /// If element has a parent it will get color of parent.
    /// </summary>
    /// <param name="charColor">Color to handle.</param>
    /// <param name="sourceCharColor">Color of the source characterMap</param>
    /// <returns>Color of parent.</returns>
    protected static CharColor HandleTransparentColor(CharColor charColor, CharColor sourceCharColor)
    {
        if (charColor == CharColor.Transparant)
        {
            charColor = sourceCharColor;
        }

        return charColor;
    }
}
