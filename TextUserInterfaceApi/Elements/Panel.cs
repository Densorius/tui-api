﻿using TextUserInterfaceApi.Interface;
using TextUserInterfaceApi.Utils;

namespace TextUserInterfaceApi.Elements;

/// <summary>
/// Represents a panel on which elements can be placed.
/// </summary>
public class Panel : Element, IParent, IInnerDimensions
{
    /// <summary>
    /// Gets or sets the width of the panel and will also set the innerWidth based on the shadow offset.
    /// </summary>
    public override int Width
    {
        get => base.Width;
        set
        {
            base.Width = value;
            InnerWidth = base.Width - Math.Abs(ShadowOffsetX);
        }
    }

    /// <summary>
    /// Gets or sets the height of the panel and will also set the innerHeight based on the shadow offset.
    /// </summary>
    public override int Height
    {
        get => base.Height;
        set
        {
            base.Height = value;
            InnerHeight = value - Math.Abs(ShadowOffsetY);
        }
    }

    /// <summary>
    /// Gets or sets the shadow offset on the X axis. 
    /// Set will also set the width of the panel based on the new shadow offset.
    /// </summary>
    public int ShadowOffsetX
    {
        get => _shadowOffsetX;
        set
        {
            _shadowOffsetX = value;
            Width = InnerWidth + Math.Abs(value);
        }
    }

    /// <summary>
    /// Gets or sets the shadow offset on the Y axis. 
    /// Set will also set the height of the panel based on the new shadow offset.
    /// </summary>
    public int ShadowOffsetY
    {
        get => _shadowOffsetY;
        set
        {
            _shadowOffsetY = value;
            Height = InnerHeight + Math.Abs(value);
        }
    }

    /// <summary>
    /// Gets or sets the fill character.
    /// </summary>
    public char FillChar { get; set; }

    /// <summary>
    /// Gets or sets the color of the shadow.
    /// </summary>
    public CharColor ShadowColor { get; set; }

    /// <summary>
    /// Gets or sets the list of child elements
    /// </summary>
    public List<Element> ChildElements { get; set; }
    public CharacterMap CharacterMap { get; private set; }
    public int InnerWidth { get; private set; }
    public int InnerHeight { get; private set; }

    private int _shadowOffsetX;
    private int _shadowOffsetY;

    /// <summary>
    /// Initializes a new panel.
    /// </summary>
    /// <param name="x">X position of the panel.</param>
    /// <param name="y">Y position of the panel.</param>
    /// <param name="width">Width of the panel.</param>
    /// <param name="height">Height of the panel.</param>
    /// <param name="fgColor">Foreground color of the panel.</param>
    /// <param name="bgColor">Background color of the panel.</param>
    public Panel(int x, int y, int width, int height, CharColor fgColor = CharColor.Black, CharColor bgColor = CharColor.Gray)
    {
        Initialize();

        X = x;
        Y = y;
        Width = width;
        InnerWidth = width - Math.Abs(ShadowOffsetX);
        //Debug.WriteLine($"CTOR: Width: {Width}");

        Height = height;
        InnerHeight = height - Math.Abs(ShadowOffsetY);
        FGColor = fgColor;
        BGColor = bgColor;
    }

    /// <summary>
    /// Initializes a new panel.
    /// </summary>
    public Panel()
    {
        X = 0;
        Y = 0;
        Width = 0;
        InnerWidth = 0;
        Height = 0;
        InnerHeight = 0;
        FGColor = CharColor.Black;
        BGColor = CharColor.Gray;

        Initialize();
    }

    public override void Update(CharacterMap characterMap)
    {
        CharacterMap = new CharacterMap(InnerWidth, InnerHeight);

        int panelX = 0;
        int panelY = 0;
        int shadowX = 0;
        int shadowY = 0;

        if (ShadowOffsetX < 0)
        {
            panelX = Math.Abs(ShadowOffsetX);
        }
        else
        {
            shadowX = ShadowOffsetX;
        }

        if (ShadowOffsetY < 0)
        {
            panelY = Math.Abs(ShadowOffsetY);
        }
        else
        {
            shadowY = ShadowOffsetY;
        }

        // render panel
        for (int y = 0; y < Height; y++)
        {
            for (int x = 0; x < Width; x++)
            {
                if (x + X < characterMap.Width && y + Y < characterMap.Height && x + X >= 0 && y + Y >= 0)
                {
                    if ((x >= panelX && x < panelX + InnerWidth) && (y >= panelY && y < panelY + InnerHeight))
                    {
                        // modify elements charmap
                        CharacterMap.Characters[y - panelY, x - panelX] = new ColorChar(FillChar, FGColor, BGColor);
                    }
                    else if ((x >= shadowX && x < (shadowX + Width)) && (y >= shadowY && y < shadowY + Height))
                    {
                        // modify given charmap (shadow)
                        characterMap.Characters[Y + y, X + x] = new ColorChar(characterMap.Characters[Y + y, X + x].Character, CharColor.DarkGray, ShadowColor);
                    }
                }
            }
        }

        ChildElements.ForEach(child =>
        {
            if (!child.Hidden)
                child.Update(CharacterMap);
        });

        characterMap.PasteMap(CharacterMap, X + panelX, Y + panelX);
    }

    /// <summary>
    /// Adds an element to the panel.
    /// </summary>
    /// <param name="element">Element to add.</param>
    public void AddElement(Element element)
    {
        element.Parent = this;
        ChildElements.Add(element);
        element.AddedToInterface();
    }

    /// <summary>
    /// Adds a variable number of elements to the panel.
    /// </summary>
    /// <param name="elements">Elements to add.</param>
    public void AddMultipleElements(params Element[] elements)
    {
        elements.ForEach(element => AddElement(element));
    }

    /// <summary>
    /// Helps the constructor initialize some parameters with a default value.
    /// </summary>
    private void Initialize()
    {
        ShadowOffsetX = 2;
        ShadowOffsetY = 1;
        ShadowColor = CharColor.Black;
        FillChar = ' ';

        ChildElements = new List<Element>();

        // modify inner width and height
        //UpdateDimensions();
        CharacterMap = new CharacterMap(InnerWidth, InnerHeight);
    }
}
