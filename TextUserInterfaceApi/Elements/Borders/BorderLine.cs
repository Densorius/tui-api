﻿namespace TextUserInterfaceApi.Elements.Borders;

public class BorderLine
{
    public int X { get; set; }
    public int Y { get; set; }
    public int Length { get; set; }
    public Axis Axis { get; set; }
    public CharColor FGColor { get; set; }
    public CharColor BGColor { get; set; }

    public BorderLine(int x, int y, int length, Axis axis, CharColor fgColor = CharColor.Black, CharColor bgColor = CharColor.Transparant)
    {
        X = x;
        Y = y;
        Length = length;
        FGColor = fgColor;
        BGColor = bgColor;
        Axis = axis;
    }
}
