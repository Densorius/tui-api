﻿namespace TextUserInterfaceApi.Elements;

public class Canvas : Element
{
    /// <summary>
    /// Current fill color for foreground. This foreground color is used when drawing something on the canvas.
    /// </summary>
    public CharColor ForegroundFillColor { get; set; }

    /// <summary>
    /// Current fill color for background. This background color is used when drawing something on the canvas.
    /// </summary>
    public CharColor BackgroundFillColor { get; set; }

    /// <summary>
    /// Current fill character. This character is used when drawing something on the canvas.
    /// </summary>
    public char FillCharacter { get; set; }

    private readonly CharacterMap _canvasMap;

    public Canvas(int x, int y, int width, int height) : this()
    {
        X = x;
        Y = y;
        Width = width;
        Height = height;

        _canvasMap = new CharacterMap(Width, Height);
    }

    public Canvas()
    {
        X = 0;
        Y = 0;
        Width = 0;
        Height = 0;

        BackgroundFillColor = CharColor.Black;
        ForegroundFillColor = CharColor.Gray;

        FillCharacter = ' ';

        _canvasMap = new CharacterMap(Width, Height);
    }

    /// <summary>
    /// Draws a single character with the current FillCharacter and FillColor on the canvas.
    /// </summary>
    /// <param name="x">X position of the character.</param>
    /// <param name="y">Y position of the character.</param>
    /// <param name="character">The character itself to draw.</param>
    public void DrawSingleCharacter(int x, int y)
    {
        if (x < Width && y < Height && x >= 0 && y >= 0)
        {
            _canvasMap.Characters[y, x] = GetCurrenctCharAndColor();
        }
    }

    /// <summary>
    /// Draws a line of characters using the current set character and colors.
    /// </summary>
    /// <param name="startX">X position of the start of the line.</param>
    /// <param name="startY">Y position of the start of the line.</param>
    /// <param name="length">Lenght of the line.</param>
    /// <param name="axis">Axis the line will be on.</param>
    public void DrawLine(int startX, int startY, int length, Axis axis)
    {
        for (int i = 0; i < length; i++)
        {
            if (axis == Axis.X && (startX + i) < Width)
            {
                _canvasMap.Characters[startY, startX + i] = GetCurrenctCharAndColor();
            }

            if (axis == Axis.Y && (startY + i) < Height)
            {
                _canvasMap.Characters[startY + i, startX] = GetCurrenctCharAndColor();
            }
        }
    }

    /// <summary>
    /// Draws a rectangle on the canvas using current set character and colors.
    /// </summary>
    /// <param name="x">X position of the rectangle.</param>
    /// <param name="y">Y position of the rectangle.</param>
    /// <param name="width">Width of the rectangle.</param>
    /// <param name="height">Height of the rectangle.</param>
    public void DrawRectangle(int x, int y, int width, int height)
    {
        for (int iy = 0; iy < height; iy++)
        {
            for (int ix = 0; ix < width; ix++)
            {
                if (x >= 0 && y >= 0 && x + ix < Width && y + iy < Height)
                {
                    _canvasMap.Characters[y + iy, x + ix] = GetCurrenctCharAndColor();
                }
            }
        }
    }

    public void Clear(CharColor charColor = CharColor.Transparant)
    {
        // store old values.
        char oldFillCharacter = FillCharacter;
        CharColor oldBackgroundColor = BackgroundFillColor;
        CharColor oldForeGroundColor = ForegroundFillColor;

        FillCharacter = ' ';
        BackgroundFillColor = charColor;
        ForegroundFillColor = charColor;

        DrawRectangle(0, 0, Width, Height);

        // restore old values.
        FillCharacter = oldFillCharacter;
        BackgroundFillColor = oldBackgroundColor;
        ForegroundFillColor = oldForeGroundColor;
    }

    public override void Update(CharacterMap characterMap)
    {
        characterMap.PasteMap(_canvasMap, X, Y);
    }

    /// <summary>
    /// Returns a new instance of the ColorChar class with the current FillCharacter and colors.
    /// </summary>
    /// <returns>ColorChar with FillCharacter and colors.</returns>
    private ColorChar GetCurrenctCharAndColor() => new(FillCharacter, ForegroundFillColor, BackgroundFillColor);
}
