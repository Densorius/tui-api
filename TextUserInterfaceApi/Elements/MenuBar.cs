﻿using TextUserInterfaceApi.Interface;
using TextUserInterfaceApi.Utils;

namespace TextUserInterfaceApi.Elements;

public class MenuBar : Element, IFocusable
{
    /// <summary>
    /// Foreground color for a highlighted menu item.
    /// </summary>
    public CharColor HighlightFGColor { get; set; }

    /// <summary>
    /// Background color for a highlighted menu item.
    /// </summary>
    public CharColor HighlightBGColor { get; set; }

    /// <summary>
    /// List of menuBarItems in the menubar.
    /// </summary>
    public SelectableList<MenuBarItem> MenuBarItems { get; set; }

    public int Offset { get; set; }
    public int Padding { get; set; }

    public int FocusLevel { get => _focusLevel; }

    /// <summary>
    /// Wether the menuBar is in focus or not. If set to true, the menubar is activated.
    /// </summary>
    public bool Focus
    {
        get => _focus;
        set
        {
            _focus = value;

            if (_focus)
                Activate();
        }
    }

    /// <summary>
    /// Key for menuBar activation.
    /// </summary>
    public ConsoleKey KeyToActivate;

    /// <summary>
    /// A secundary key for menubar activation. This key bypasses the modifier key requirement.
    /// </summary>
    public ConsoleKey SecundaryKeyToActivate;

    public List<ConsoleModifiers> ModifiersForActivation;

    private bool _focus;
    private bool _menuVisible;

    private int _focusLevel;

    public MenuBar(int y, CharColor fgColor, CharColor bgColor)
    {
        Y = y;
        Initialize(fgColor, bgColor);
    }

    public MenuBar(CharColor fgColor, CharColor bgColor)
    {
        Y = 0;
        Initialize(fgColor, bgColor);
    }

    public override void Update(CharacterMap characterMap)
    {
        X = 0;
        Width = characterMap.Width;
        Height = characterMap.Height - 2;

        int charactersLeftToFill = characterMap.Width;
        int currentIndex = 0;

        UpdateMenuPositions();
        List<ColorString> colorStrings = CompileColorStrings();

        colorStrings.ForEach(colorString =>
        {
            charactersLeftToFill -= colorString.Str.Length;

            for (int i = 0; i < colorString.Str.Length; i++)
            {
                characterMap.Characters[Y, currentIndex + i] = new ColorChar(colorString.Str[i], colorString.FGColor, colorString.BGColor);
            }
            currentIndex += colorString.Str.Length;
        });

        for (int i = 0; i < charactersLeftToFill; i++)
        {
            characterMap.Characters[Y, currentIndex + i] = new ColorChar(' ', FGColor, BGColor);
        }

        if (_focus && MenuBarItems.Selected.IsActive == true)
        {
            MenuBarItems.Selected.MenuPanel.Update(characterMap);
        }
    }

    public override void KeyPressTriggered(ConsoleKeyInfo keyInfo)
    {
        if (KeyToActivate == keyInfo.Key || SecundaryKeyToActivate == keyInfo.Key)
        {
            int modiefiersActive = 0;

            ModifiersForActivation.ForEach(modifier =>
            {
                if (keyInfo.Modifiers.HasFlag(modifier))
                {
                    modiefiersActive++;
                }
            });

            if (ModifiersForActivation.Count == 0 || modiefiersActive == ModifiersForActivation.Count || SecundaryKeyToActivate == keyInfo.Key)
            {
                Activate();
            }
        }

        if (_focus)
        {
            if (_menuVisible && MenuBarItems.Selected.IsActive == true)
            {
                MenuBarItems.Selected.Menu.KeyPressTriggered(keyInfo);
            }

            switch (keyInfo.Key)
            {
                case ConsoleKey.LeftArrow:
                    MenuBarItems.DecreaseIndex();
                    if (_menuVisible)
                    {
                        ShowMenu();
                    }
                    break;

                case ConsoleKey.RightArrow:
                    MenuBarItems.IncreaseIndex();
                    if (_menuVisible)
                    {
                        ShowMenu();
                    }
                    break;

                case ConsoleKey.Escape:
                    _focus = false;
                    _menuVisible = false;
                    MenuBarItems.Items.ForEach(item => item.IsActive = false);
                    break;

                case ConsoleKey.DownArrow:
                case ConsoleKey.Enter:
                    if (!_menuVisible)
                    {
                        ShowMenu();
                    }
                    break;
            }
        }
    }

    public override void SetColor(CharColor fgColor, CharColor bgColor)
    {
        FGColor = fgColor;
        BGColor = bgColor;

        HighlightFGColor = bgColor;
        HighlightBGColor = fgColor;
    }

    /// <summary>
    /// Adds a menuBarItem to the menuBar.
    /// </summary>
    /// <param name="menuBarItem">MenuBarItem to add.</param>
    public void AddMenuBarItem(MenuBarItem menuBarItem)
    {
        MenuBarItems.Add(menuBarItem);
    }

    private void Initialize(CharColor fgColor, CharColor bgColor)
    {
        X = 0;
        Offset = 2;
        Padding = 1;
        _focusLevel = int.MaxValue;
        SetColor(fgColor, bgColor);
        KeyToActivate = ConsoleKey.F1; // default
        ModifiersForActivation = new List<ConsoleModifiers>();

        MenuBarItems = new SelectableList<MenuBarItem>();
    }

    private void Activate()
    {
        MenuBarItems.SetIndexToFirst();
        _focus = !_focus;
    }

    private void UpdateMenuPositions()
    {
        int currentX = Offset;

        MenuBarItems.Items.ForEach(item =>
        {
            item.X = currentX;

            currentX += item.Name.Length + Padding * 2;
        });
    }

    private List<ColorString> CompileColorStrings()
    {
        List<ColorString> colorStrings = new();

        StringBuilder builder = new();

        for (int j = 0; j < Offset; j++)
        {
            builder.Append(' ');
        }

        for (int i = 0; i < MenuBarItems.Count; i++)
        {
            string currentMenuName = MenuBarItems[i].Name;

            if (Focus && i == MenuBarItems.SelectedIndex)
            {
                colorStrings.Add(new ColorString(builder.ToString(), FGColor, BGColor));
                builder.Clear();
            }

            // loop through string of current menuBarItem with classic for loop to add set padding in one loop
            for (int j = 0; j < currentMenuName.Length + (Padding * 2); j++)
            {
                if (j < Padding || j > Padding + currentMenuName.Length - 1)
                {
                    builder.Append(' ');
                }
                else
                {
                    builder.Append(currentMenuName[j - Padding]);
                }
            }

            if (Focus && i == MenuBarItems.SelectedIndex)
            {
                colorStrings.Add(new ColorString(builder.ToString(), HighlightFGColor, HighlightBGColor));
                builder.Clear();
            }
        }

        colorStrings.Add(new ColorString(builder.ToString(), FGColor, BGColor));

        return colorStrings;
    }

    private void ShowMenu()
    {
        MenuBarItems.Items.ForEach(item => item.IsActive = false);

        MenuBarItems.Selected.IsActive = true;
        MenuBarItems.Selected.Menu.MenuItems.SetIndexToFirst();
        _menuVisible = true;
    }
}
