﻿using TextUserInterfaceApi.Elements.Borders;
using TextUserInterfaceApi.Utils;

namespace TextUserInterfaceApi.Elements;

/// <summary>
/// Represents a border. Borders can have lines defined how
/// </summary>
public class Border : Element
{
    /// <summary>
    /// Array with characters used to draw the border.
    /// </summary>
    public char[] BorderCharacters { get; set; }

    /// <summary>
    /// List of border lines in the border.
    /// </summary>
    public List<BorderLine> BorderLines { get; set; }

    private Dictionary<byte, int> _borderCharacterIds;
    private List<List<BorderCell>> _borderCellMap;

    public Border()
    {
        X = 0;
        Y = 0;
        Width = 0;
        Height = 0;

        Initialize();
    }

    public Border(int x, int y, int width, int height, CharColor fgColor = CharColor.Black, CharColor bgColor = CharColor.Transparant)
    {
        X = x;
        Y = y;
        Width = width;
        Height = height;
        FGColor = fgColor;
        BGColor = bgColor;

        Initialize();
    }

    public override void Update(CharacterMap characterMap)
    {
        UpdateBorderMap();

        for (int y = 0; y < Height; y++)
        {
            for (int x = 0; x < Width; x++)
            {
                if (CheckWithinDimensions(characterMap, y, x))
                {
                    if (_borderCellMap[y][x] != null)
                    {
                        CharColor fgColor = HandleTransparentColor(FGColor, characterMap.Characters[Y + y, X + x].FGColor);
                        CharColor bgColor = HandleTransparentColor(BGColor, characterMap.Characters[Y + y, X + x].BGColor);

                        int borderCharacterId = _borderCellMap[y][x].BorderCharacterId;

                        characterMap.Characters[Y + y, X + x] = new ColorChar(BorderCharacters[borderCharacterId], fgColor, bgColor);
                    }
                }
            }
        }
    }

    private bool CheckWithinDimensions(CharacterMap characterMap, int y, int x)
    {
        return x + X < characterMap.Width && y + Y < characterMap.Height && x + X >= 0 && y + Y >= 0;
    }

    /// <summary>
    /// Creates a border the size of the element.
    /// </summary>
    public void CreateFullSizeBorder()
    {
        BorderLines.Add(new BorderLine(0, 0, Width, Axis.X));
        BorderLines.Add(new BorderLine(0, 0, Height, Axis.Y));
        BorderLines.Add(new BorderLine(0, Height - 1, Width, Axis.X));
        BorderLines.Add(new BorderLine(Width - 1, 0, Width, Axis.Y));

        UpdateBorderMap();
    }

    /// <summary>
    /// Clears the list of borderlines. This is usefull when updating the size of the border.
    /// </summary>
    public void ClearBorder()
    {
        BorderLines.Clear();
    }

    /// <summary>
    /// Helps the constructer to initialize the border.
    /// </summary>
    private void Initialize()
    {
        BorderCharacters = new char[] { '─', '│', '┌', '┬', '┐', '├', '┼', '┤', '└', '┴', '┘' };

        // key of dictionary is a boolean array in byte form. The booleans are in order of { top, right, bottom, left }
        _borderCharacterIds = new Dictionary<byte, int>()
            {
                { 0b0110, 2 },
                { 0b0111, 3 },
                { 0b0011, 4 },
                { 0b1110, 5 },
                { 0b1111, 6 },
                { 0b1011, 7 },
                { 0b1100, 8 },
                { 0b1101, 9 },
                { 0b1001, 10 }
            };

        _borderCellMap = new List<List<BorderCell>>();

        // fill the border cell map with nulls
        for (int y = 0; y < Height; y++)
        {
            _borderCellMap.Add(new List<BorderCell>());

            for (int x = 0; x < Width; x++)
            {
                _borderCellMap[y].Add(null);
            }
        }

        BorderLines = new List<BorderLine>();
    }

    /// <summary>
    /// Updates the border map by going though each border line and 
    /// </summary>
    private void UpdateBorderMap()
    {
        // go through the borderlines first
        CreateBorderLines();

        // go through the borderCells to update their character array id in case they have neighbours
        HandleBorderNeighbours();
    }

    /// <summary>
    /// Returns a boolean array corrosponding with where a neighbour is in the order of { top, right, bottom, left }.
    /// </summary>
    /// <param name="x">X position of cell.</param>
    /// <param name="y">Y position of cell.</param>
    /// <returns>Array of four booleans that tell where a neigbour is. ({ top, right, bottom, left })</returns>
    private bool[] GetNeighbourBorders(int x, int y)
    {
        // { top, right, bottom, left }
        bool[] neighbours = { false, false, false, false };

        if (y > 0 && _borderCellMap[y - 1][x] != null)
            neighbours[0] = true;

        if ((x + 1) < Width && _borderCellMap[y][x + 1] != null)
            neighbours[1] = true;

        if ((y + 1) < Height && _borderCellMap[y + 1][x] != null)
            neighbours[2] = true;

        if (x > 0 && _borderCellMap[y][x - 1] != null)
            neighbours[3] = true;

        return neighbours;
    }

    /// <summary>
    /// Represents a border cell.
    /// </summary>
    private class BorderCell
    {
        /// <summary>
        /// X position of the border cell.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Y position of the border cell.
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Border character id corrosponding with the array of border characters.
        /// </summary>
        public int BorderCharacterId { get; set; }

        /// <summary>
        /// Initializes the border cell.
        /// </summary>
        /// <param name="x">X position of the border cell.</param>
        /// <param name="y">Y position of the border cell.</param>
        /// <param name="borderCharacterId">Border character id.</param>
        public BorderCell(int x, int y, int borderCharacterId)
        {
            X = x;
            Y = y;
            BorderCharacterId = borderCharacterId;
        }
    }

    private void CreateBorderLines()
    {
        BorderLines.ForEach(borderLine =>
        {
            if (borderLine.Axis == Axis.X)
            {
                int y = borderLine.Y;

                for (int x = 0; x < Width; x++)
                {
                    _borderCellMap[y][x] = new BorderCell(x, y, 0);
                }
            }
            else if (borderLine.Axis == Axis.Y)
            {
                int x = borderLine.X;

                for (int y = 0; y < Height; y++)
                {
                    _borderCellMap[y][x] = new BorderCell(x, y, 1);
                }
            }
        });
    }

    private void HandleBorderNeighbours()
    {
        for (int y = 0; y < Height; y++)
        {
            for (int x = 0; x < Width; x++)
            {
                byte neighboursAsByte = BooleanArrayUtils.BooleanArrayToByte(GetNeighbourBorders(x, y));

                if (_borderCharacterIds.ContainsKey(neighboursAsByte) && _borderCellMap[y][x] != null)
                {
                    _borderCellMap[y][x].BorderCharacterId = _borderCharacterIds[neighboursAsByte];
                }
            }
        }
    }
}
