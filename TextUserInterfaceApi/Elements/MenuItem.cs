﻿namespace TextUserInterfaceApi.Elements;

/// <summary>
/// Represends a menu item to be used in a menu.
/// </summary>
public class MenuItem
{
    /// <summary>
    /// Text of the menu item.
    /// </summary>
    public string Text { get; set; }

    /// <summary>
    /// Fired when the menu item is activated.
    /// </summary>
    public event EventHandler<EventArgs> Activated;

    /// <summary>
    /// Initializes the menu item.
    /// </summary>
    /// <param name="text">Text the menu item should have.</param>
    public MenuItem(string text)
    {
        Text = text;
    }

    /// <summary>
    /// Launches the Activated event
    /// </summary>
    public void Activate()
    {
        Activated?.Invoke(this, new EventArgs());
    }
}
