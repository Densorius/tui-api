﻿using TextUserInterfaceApi.Event;
using TextUserInterfaceApi.Interface;
using TextUserInterfaceApi.Utils;

namespace TextUserInterfaceApi.Elements;

/// <summary>
/// A rectangular area on which elements can be placed.
/// </summary>
public class Container : Element, IParent
{
    /// <summary>
    /// Fill character.
    /// </summary>
    public char FillChar { get; set; }

    /// <summary>
    /// List of elements as children of the panel.
    /// </summary>
    public List<Element> ChildElements { get; set; }

    /// <summary>
    /// CharacterMap of the panel.
    /// </summary>
    public CharacterMap CharacterMap { get; private set; }

    /// <summary>
    /// EventHandler that fires when a key was pressed.
    /// </summary>
    public event EventHandler<KeyPressedArgs> KeyPressed;

    /// <summary>
    /// Container for hosting elements in the user interface.
    /// </summary>
    public Container() : base()
    {
        ChildElements = new List<Element>();
    }

    /// <summary>
    /// Container with a specific width and height for hosting elements in the user interface.
    /// </summary>
    public Container(int width, int height, CharColor fgColor = CharColor.Gray, CharColor bgColor = CharColor.DarkBlue) : base()
    {
        Initialize(fgColor, bgColor, width, height);
    }

    /// <summary>
    /// Container with a specific size and position for hosting elements in the user interface.
    /// </summary>
    public Container(int x, int y, int width, int height, CharColor fgColor = CharColor.Gray, CharColor bgColor = CharColor.DarkBlue)
    {
        X = x;
        Y = y;

        Initialize(fgColor, bgColor, width, height);
    }

    /// <summary>
    /// Modifies the character map given.
    /// </summary>
    /// <param name="characterMap">Character map to modify.</param>
    public override void Update(CharacterMap characterMap)
    {
        for (int y = 0; y < CharacterMap.Height; y++)
        {
            for (int x = 0; x < CharacterMap.Width; x++)
            {
                CharacterMap.Characters[y, x].BGColor = BGColor;
                CharacterMap.Characters[y, x].FGColor = FGColor;
                CharacterMap.Characters[y, x].Character = FillChar;
            }
        }

        foreach (Element child in ChildElements)
        {
            if (!child.Hidden)
                child.Update(CharacterMap);
        }

        characterMap.PasteMap(CharacterMap, X, Y);
    }

    /// <summary>
    /// Adds the provided element as a child of the current element.
    /// </summary>
    /// <param name="element">element to add</param>
    public void AddElement(Element element)
    {
        element.Parent = this;

        ChildElements.Add(element);

        element.AddedToInterface();
    }

    /// <summary>
    /// Adds the provided elements as children of the current element.
    /// </summary>
    /// <param name="elements">Elements to add.</param>
    public void AddMultipleElements(params Element[] elements)
    {
        elements.ForEach(element => AddElement(element));
    }

    /// <summary>
    /// Handles any key being pressed.
    /// </summary>
    /// <param name="keyInfo"></param>
    public override void KeyPressTriggered(ConsoleKeyInfo keyInfo)
    {
        KeyPressed?.Invoke(this, new KeyPressedArgs(keyInfo));
    }

    /// <summary>
    /// Resets the character map with another width and height.
    /// </summary>
    /// <param name="width">New width.</param>
    /// <param name="height">New height.</param>
    public void ResetCharacterMap(int width, int height)
    {
        CharacterMap = new CharacterMap(width, height);
    }

    private void Initialize(CharColor fgColor, CharColor bgColor, int width, int height)
    {
        FGColor = fgColor;
        BGColor = bgColor;
        Width = width;
        Height = height;

        FillChar = ' ';

        ChildElements = new List<Element>();
        CharacterMap = new CharacterMap(width, height);
    }
}
