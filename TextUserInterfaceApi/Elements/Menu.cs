﻿using System.Diagnostics;
using TextUserInterfaceApi.Interface;
using TextUserInterfaceApi.Utils;

namespace TextUserInterfaceApi.Elements;

/// <summary>
/// Represents a menu.
/// </summary>
public class Menu : Element, IFocusable
{
    /// <summary>
    /// Foreground color for a highlighted menu item.
    /// </summary>
    public CharColor HighlightFGColor { get; set; }

    /// <summary>
    /// Background color for a highlighted menu item.
    /// </summary>
    public CharColor HighlightBGColor { get; set; }

    /// <summary>
    /// Minumum width the menu should have.
    /// </summary>
    public int MinWidth { get; set; }

    public int Padding { get; set; }

    public int FocusLevel { get => _focusLevel; }

    /// <summary>
    /// List of menu items.
    /// </summary>
    public SelectableList<MenuItem> MenuItems { get; set; }

    /// <summary>
    /// Whether the menu is in focus;
    /// </summary>
    public bool Focus { get; set; }

    override public int Width
    {
        get => _width;
        set
        {
            if (value < MinWidth)
                _width = MinWidth;
            else
                _width = value;
        }
    }

    private int _width;
    private int _focusLevel;

    /// <summary>
    /// Initializes the menu.
    /// </summary>
    /// <param name="x">X position of the menu.</param>
    /// <param name="y">Y position of the menu.</param>
    /// <param name="minWidth">Minumum width the menu should have.</param>
    /// <param name="fgColor">Foreground color.</param>
    /// <param name="bgColor">Background color.</param>
    public Menu(int x, int y, int minWidth, CharColor fgColor = CharColor.Black, CharColor bgColor = CharColor.Gray)
    {
        Initialize();

        FGColor = fgColor;
        BGColor = bgColor;

        X = x;
        Y = y;
        Width = minWidth;
        MinWidth = minWidth;

        // invert fgColor and bgColor for highlight
        HighlightBGColor = fgColor;
        HighlightFGColor = bgColor;
    }

    /// <summary>
    /// Initializes the menu.
    /// </summary>
    /// <param name="fgColor">Foreground color.</param>
    /// <param name="bgColor">Background color.</param>
    public Menu(CharColor fgColor = CharColor.Black, CharColor bgColor = CharColor.Gray)
    {
        Initialize();

        FGColor = fgColor;
        BGColor = bgColor;

        X = 0;
        Y = 0;
        Width = 0;
        MinWidth = 0;

        // invert fgColor and bgColor for highlight
        HighlightBGColor = fgColor;
        HighlightFGColor = bgColor;
    }

    public override void SetColor(CharColor fgColor, CharColor bgColor)
    {
        FGColor = fgColor;
        BGColor = bgColor;

        HighlightFGColor = bgColor;
        HighlightBGColor = fgColor;
    }

    public override void Update(CharacterMap characterMap)
    {
        Height = MenuItems.Count;

        for (int y = 0; y < Height; y++)
        {
            char[] currentMenuItemCharArray = CreateMenuItemString(y).ToCharArray();

            for (int x = 0; x < Width; x++)
            {
                if (x < currentMenuItemCharArray.Length)
                {
                    CharColor fgColor = HandleTransparentColor(FGColor, characterMap.Characters[Y + y, X + x].FGColor);
                    CharColor bgColor = HandleTransparentColor(BGColor, characterMap.Characters[Y + y, X + x].BGColor);
                    CharColor highlightFGColor = HandleTransparentColor(HighlightFGColor, characterMap.Characters[Y + y, X + x].BGColor);
                    CharColor highlightBGColor = HandleTransparentColor(HighlightBGColor, characterMap.Characters[Y + y, X + x].FGColor);

                    if (y == MenuItems.SelectedIndex)
                    {
                        characterMap.Characters[Y + y, X + x] = new ColorChar(currentMenuItemCharArray[x], highlightFGColor, highlightBGColor);
                    }
                    else
                    {
                        characterMap.Characters[Y + y, X + x] = new ColorChar(currentMenuItemCharArray[x], fgColor, bgColor);
                    }
                }
            }
        }
    }

    public override void KeyPressTriggered(ConsoleKeyInfo keyInfo)
    {
        if (MenuItems.Count > 0 && MenuItems.Items.Where(item => item != null).Any())
        {
            switch (keyInfo.Key)
            {
                case ConsoleKey.UpArrow:
                    MenuItems.DecreaseIndex();
                    break;

                case ConsoleKey.DownArrow:
                    MenuItems.IncreaseIndex();
                    break;

                case ConsoleKey.Home:
                    // Move to first menuItem
                    MenuItems.SetIndexToFirst();
                    break;

                case ConsoleKey.End:
                    // Move to last menuItem
                    MenuItems.SetIndexToLast();
                    break;

                case ConsoleKey.Enter:
                    MenuItems.Selected.Activate();
                    break;
            }
        }
        else
        {
            Debug.WriteLine("WARNING: MenuItems list only contains null items and therfore no item can be selected.");
        }
    }

    /// <summary>
    /// Adds a menuItem to the menu.
    /// </summary>
    /// <param name="menuItem">MenuItem to add.</param>
    public void AddMenuItem(MenuItem menuItem)
    {
        MenuItems.Items.Add(menuItem);
        MenuItems.SetIndexToFirst();

        int newWidth = GetLongestMenuItemLenght();

        if (newWidth > MinWidth)
        {
            Width = newWidth;
        }

        Height++;
    }

    public void AddMenuItems(params MenuItem[] menuItems)
    {
        menuItems.ForEach(item => AddMenuItem(item));
    }

    /// <summary>
    /// Adds multiple menuItems at once.
    /// </summary>
    /// <param name="menuItems">MenuItems to add.</param>
    public void AddMultipleMenuItems(params MenuItem[] menuItems)
    {
        menuItems.ForEach(menuItem => AddMenuItem(menuItem));
    }

    private void Initialize()
    {
        Height = 0;
        _focusLevel = 1;
        Padding = 1;
        MenuItems = new SelectableList<MenuItem>();
    }

    /// <summary>
    /// Builds a string to use to display the menuItem.
    /// </summary>
    /// <param name="MenuItemIndex">Index of the menuItem</param>
    /// <returns>Build string</returns>
    private string CreateMenuItemString(int MenuItemIndex)
    {
        StringBuilder builder = new();
        int width = 0;
        string menuItemText;

        if (MenuItems[MenuItemIndex] != null)
            menuItemText = MenuItems[MenuItemIndex].Text;
        else
            menuItemText = string.Empty;

        for (int i = 0; i < Padding; i++)
        {
            builder.Append(' ');
            width += 1;
        }

        builder.Append(menuItemText);
        width += menuItemText.Length + Padding;

        for (int i = 0; i < Width - width + Padding; i++)
        {
            builder.Append(' ');
        }

        return builder.ToString();
    }

    /// <summary>
    /// Gets the lenght of the longest menu item
    /// </summary>
    /// <returns>Lenght of longest menu item</returns>
    private int GetLongestMenuItemLenght()
    {
        int newWidth = 0;

        MenuItems.Items.ForEach(item =>
        {
            if (item != null)
            {
                int textLenghtPlusPadding = item.Text.Length + Padding * 2;

                if (newWidth < textLenghtPlusPadding)
                {
                    newWidth = textLenghtPlusPadding;
                }
            }
        });

        return newWidth;
    }
}
