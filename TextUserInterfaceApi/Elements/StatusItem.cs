﻿namespace TextUserInterfaceApi.Elements;

/// <summary>
/// Represents a status item for a statusbar.
/// </summary>
public class StatusItem
{
    /// <summary>
    /// Gets or sets the text of the statusItem.
    /// </summary>
    public string Text { get; set; }

    /// <summary>
    /// Gets or sets the right margin of the text.
    /// </summary>
    public int MarginRight { get; set; }

    /// <summary>
    /// Gets or sets the left margin of the text.
    /// </summary>
    public int MarginLeft { get; set; }

    /// <summary>
    /// Sets both the right and left margin of the text.
    /// </summary>
    public int Margin
    {
        set
        {
            MarginLeft = value;
            MarginRight = value;
        }
    }

    /// <summary>
    /// Gets the text with the margin included.
    /// </summary>
    public string TextIncludingMargin
    {
        get => GetTextIncludingMargin();
    }

    /// <summary>
    /// Initializes a new statusItem with a default margin of one on both sides.
    /// </summary>
    /// <param name="text">Text the statusItem should have.</param>
    /// <param name="marginLeft">Left margin of the text.</param>
    /// <param name="marginRight">Right margin of the text.</param>
    public StatusItem(string text, int marginLeft = 1, int marginRight = 1)
    {
        Text = text;
        MarginLeft = marginLeft;
        MarginRight = marginRight;
    }

    /// <summary>
    /// Adds spaces to original text as margin, then returns the new string.
    /// </summary>
    /// <returns>Text with spaces added</returns>
    private string GetTextIncludingMargin()
    {
        StringBuilder builder = new();

        for (int i = 0; i < MarginLeft; i++)
            builder.Append(' ');

        builder.Append(Text);

        for (int i = 0; i < MarginRight; i++)
            builder.Append(' ');

        return builder.ToString();
    }
}
