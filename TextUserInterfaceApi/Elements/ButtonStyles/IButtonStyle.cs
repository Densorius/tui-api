﻿namespace TextUserInterfaceApi.Elements.ButtonStyles;

public interface IButtonStyle
{
    CharColor ForegroundColor { get; set; }
    CharColor BackgroundColor { get; set; }

    CharColor HighlightForegroundColor { get; set; }
    CharColor HighlightBackgroundColor { get; set; }

    bool Focused { get; set; }

    CharacterMap Draw(CharacterMap characterMap, string text, bool focus);
}
