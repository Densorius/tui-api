﻿using TextUserInterfaceApi.Utils;

namespace TextUserInterfaceApi.Elements.ButtonStyles;

class ButtonArrowStyle : IButtonStyle
{
    public CharColor ForegroundColor { get; set; }
    public CharColor BackgroundColor { get; set; }

    public CharColor HighlightForegroundColor { get; set; }
    public CharColor HighlightBackgroundColor { get; set; }

    public bool Focused { get; set; }

    public CharColor ArrowColor { get; set; }

    public char ArrowCharLeft { get; set; }
    public char ArrowCharRight { get; set; }

    public bool Shadow { get; set; }

    public ButtonArrowStyle()
    {
        ArrowCharLeft = '►';
        ArrowCharRight = '◄';

        Shadow = true;
    }

    public CharacterMap Draw(CharacterMap characterMap, string text, bool focused)
    {
        int width = characterMap.Width;
        int height = characterMap.Height;
        //int textYPosition = height / 2 - 1; // find center of button to place text there.

        int innerWidth = Shadow ? width - 1 : width;
        int innerHeight = Shadow ? height - 1 : height;

        List<char[]> content = new List<char[]>();

        foreach (string line in text.Split('\n'))
        {
            content.Add(StringUtils.AlignString(line, innerWidth, TextAlignment.CENTER).ToCharArray());
        }

        int lines = content.Count;
        int middle = lines / 2;

        //char[] content = StringUtils.AlignString(text, innerWidth, TextAlignment.CENTER).ToCharArray();

        if (focused)
        {
            content[middle][0] = ArrowCharLeft;
            content[middle][innerWidth - 1] = ArrowCharRight;
        }

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                ColorChar currentColorChar = characterMap.Characters[y, x];

                if (y < innerHeight && y < content.Count && x < innerWidth)
                {
                    characterMap.Characters[y, x] = new ColorChar(content[y][x], ForegroundColor, BackgroundColor);
                }
                else if (x < innerWidth)
                {
                    characterMap.Characters[y, x] = new ColorChar(' ', ForegroundColor, BackgroundColor);
                }

                if (Shadow)
                {
                    // The following code renders the shadow. 
                    // Height of the button does not matter as long as it's bigger than one.

                    if (height > 1)
                    {
                        if (y == 0 && x == width - 1)
                        {
                            characterMap.Characters[y, x] = new ColorChar('▄', currentColorChar.FGColor, currentColorChar.BGColor);
                        }
                    }

                    if (height > 2 && y != 0 && y != height - 1 && x == width - 1)
                    {
                        characterMap.Characters[y, x] = new ColorChar('█', currentColorChar.FGColor, currentColorChar.BGColor);
                    }

                    if (height > 1 && y == height - 1)
                    {
                        if (x != 0)
                        {
                            characterMap.Characters[y, x] = new ColorChar('▀', currentColorChar.FGColor, currentColorChar.BGColor);
                        }
                        else
                        {
                            characterMap.Characters[y, x] = new ColorChar(currentColorChar.Character, currentColorChar.FGColor, currentColorChar.BGColor);
                        }
                    }
                }
            }
        }

        return characterMap;
    }
}
