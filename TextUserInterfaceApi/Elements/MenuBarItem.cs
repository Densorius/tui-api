﻿using TextUserInterfaceApi.Elements.Borders;

namespace TextUserInterfaceApi.Elements;

/// <summary>
/// Represends a menu bar item containing a panel, border and the menu itself.
/// </summary>
public class MenuBarItem
{
    /// <summary>
    /// Panel the menu appears in.
    /// </summary>
    public Panel MenuPanel { get; set; }

    /// <summary>
    /// Menu border above the menu. With added lines where a menu item is null.
    /// </summary>
    public Border MenuBorder { get; set; }

    /// <summary>
    /// Whether the menu is acitve or not. The menu will only be drawn if this value is set to true
    /// </summary>
    public bool IsActive { get; set; }

    public string Name { get; set; }

    /// <summary>
    /// The menu itself. When assigned this will cause the menu to be rebuild.
    /// </summary>
    public Menu Menu
    {
        get => _menu;
        set
        {
            _menu = value;
            BuildMenu();
        }
    }

    /// <summary>
    /// X position of the panel. 
    /// Changing this value will also set it as the position of the panel.
    /// </summary>
    public int X
    {
        get => _x;
        set
        {
            _x = value;
            MenuPanel.X = value;
        }
    }

    /// <summary>
    /// Y position of the panel.
    /// Changing this value will also set it as the position of the panel.
    /// </summary>
    public int Y
    {
        get => _y;
        set
        {
            _y = value;
            MenuPanel.Y = value;
        }
    }

    private int _x;
    private int _y;
    private Menu _menu;

    /// <summary>
    /// Initializes the menuBarItem with a menu.
    /// </summary>
    public MenuBarItem(Menu menu, string name)
    {
        Menu = menu;
        Name = name;

        BuildMenu();
    }

    /// <summary>
    /// (Re)builds the menu.
    /// </summary>
    public void BuildMenu()
    {
        Menu.X = 1;
        Menu.Y = 1;

        MenuPanel = new Panel(X, Y + 1, Menu.Width + Menu.X + 3, Menu.Height + 3);
        MenuBorder = new Border(0, 0, MenuPanel.InnerWidth, MenuPanel.InnerHeight);
        MenuBorder.CreateFullSizeBorder();

        // loop through the menuItems in the menu to determine where a divider needs to be placed
        for (int i = 0; i < Menu.MenuItems.Count; i++)
        {
            if (Menu.MenuItems[i] == null)
            {
                // place divider
                MenuBorder.BorderLines.Add(new BorderLine(MenuBorder.X + 1, i + 1, MenuBorder.Width - 2, Axis.X));
            }
        }

        MenuPanel.AddMultipleElements(Menu, MenuBorder);
    }
}
