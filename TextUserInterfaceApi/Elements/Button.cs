﻿using TextUserInterfaceApi.Elements.ButtonStyles;
using TextUserInterfaceApi.Event;

namespace TextUserInterfaceApi.Elements;

class Button : Element
{
    public string Text { get; set; }
    public int InnerWidth { get; set; }
    public int InnerHeight { get; set; }
    public int MinWidth { get; set; }
    public int MinHeight { get; set; }
    public int TextPaddingLeft { get; set; }
    public int TextPaddingRight { get; set; }

    public bool Focused { get; set; }

    public IButtonStyle ButtonStyle { get; set; }

    public override int Width
    {
        get => _width;
        set
        {
            _width = value;

            _characterMap = new CharacterMap(value, Height);
        }
    }

    public override int Height
    {
        get => _height;
        set
        {
            _height = value;

            _characterMap = new CharacterMap(Width, value);
        }
    }

    public ConsoleKey KeyToActivate;

    public event EventHandler<KeyPressedArgs> Clicked;

    private int _width;
    private int _height;

    private CharacterMap _characterMap;

    public Button()
    {
        MinWidth = 4;
        MinHeight = 2;

        Height = 2;

        KeyToActivate = ConsoleKey.Enter;

        _characterMap = new CharacterMap(Width, Height);
        ButtonStyle = new ButtonArrowStyle() { BackgroundColor = CharColor.White, ForegroundColor = CharColor.Black };
    }

    public override void SetSize(int width, int height)
    {
        base.SetSize(width, height);

        _characterMap = new CharacterMap(Width, Height);
    }

    public override void KeyPressTriggered(ConsoleKeyInfo keyInfo)
    {
        if (Focused && keyInfo.Key == KeyToActivate)
        {
            Clicked?.Invoke(this, new KeyPressedArgs(keyInfo));
        }
    }

    public override void Update(CharacterMap characterMap)
    {
        for (int y = 0; y < _characterMap.Height; y++)
        {
            for (int x = 0; x < _characterMap.Width; x++)
            {
                if (X + x > 0 && X + x < characterMap.Width && Y + y > 0 && Y + y < characterMap.Height)
                {
                    ColorChar[,] characters = characterMap.Characters;

                    _characterMap.Characters[y, x] = new ColorChar(
                        characters[Y + y, X + x].Character,
                        characters[Y + y, X + x].FGColor,
                        characters[Y + y, X + x].BGColor
                    );
                }
            }
        }

        _characterMap = ButtonStyle.Draw(_characterMap, Text, Focused);

        characterMap.PasteMap(_characterMap, X, Y);
    }
}
