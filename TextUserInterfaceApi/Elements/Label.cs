﻿using TextUserInterfaceApi.Utils;

namespace TextUserInterfaceApi.Elements;

public class Label : Element
{
    public string Text
    {
        get => _text;
        set
        {
            _text = value;

            if (MaxWidth == null)
            {
                _fullText = BreakString(_text, int.MaxValue);
            }
            else
            {
                _fullText = BreakString(_text, MaxWidth.Value);
            }

            UpdateSize();
        }
    }

    public int? MaxWidth { get; set; }
    public int? MaxHeight { get; set; }
    public TextAlignment Alignment { get; set; }

    private List<string> _fullText;

    private string _text;

    /// <summary>
    /// Initializes a new Label with text and a position.
    /// </summary>
    /// <param name="text">Text for the label.</param>
    /// <param name="x">X position of the label.</param>
    /// <param name="y">Y position of the label.</param>
    /// <param name="fgColor">Foreground color of the label.</param>
    /// <param name="bgColor">Background color of the label.</param>
    public Label(string text, int x, int y, CharColor fgColor = CharColor.Black, CharColor bgColor = CharColor.Transparant)
    {
        Text = text;
        X = x;
        Y = y;
        FGColor = fgColor;
        BGColor = bgColor;

        Initialize();
    }

    public Label()
    {
        Text = string.Empty;
        X = 0;
        Y = 0;
        FGColor = CharColor.Black;
        BGColor = CharColor.Transparant;

        Initialize();
    }

    public override void Update(CharacterMap characterMap)
    {
        MaxWidth = characterMap.Width - X;
        MaxHeight = characterMap.Height - Y;
        _fullText = BreakString(Text, MaxWidth.Value);
        UpdateSize();

        for (int y = 0; y < Height; y++)
        {
            if (Y + y < characterMap.Height)
            {
                string line = StringUtils.AlignString(_fullText[y], Width, Alignment);

                for (var x = 0; x < line.Length; x++)
                {
                    if (X + x < characterMap.Width)
                    {
                        CharColor fgColor = HandleTransparentColor(FGColor, characterMap.Characters[Y + y, X + x].FGColor);
                        CharColor bgColor = HandleTransparentColor(BGColor, characterMap.Characters[Y + y, X + x].BGColor);

                        characterMap.Characters[Y + y, X + x] = new ColorChar(line[x], fgColor, bgColor);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Helps the constructor to initialize properties.
    /// </summary>
    private void Initialize()
    {
        Alignment = TextAlignment.LEFT;

        _fullText = new List<string>();
        UpdateSize();
    }

    /// <summary>
    /// Updates the width and height properties with the new size.
    /// </summary>
    private void UpdateSize()
    {
        if (_fullText != null)
        {
            Width = StringUtils.GetLongestStringLengthInList(_fullText);
            Height = _fullText.Count;
        }
    }

    /// <summary>
    /// Breaks the string at appropriate places when the lenght of the string is larger than the specified max width.
    /// </summary>
    /// <param name="str">String to break.</param>
    /// <param name="maxWidth">Maximum width of the text.</param>
    /// <returns>List of strings broken into appropriate pieces.</returns>
    private List<string> BreakString(string str, int maxWidth)
    {
        List<string> strings = new();

        StringBuilder pendingString = new(); // The current sentence being constructed
        StringBuilder pendingWord = new(); // The current word being constructed

        foreach (char ch in str)
        {
            if (ch == '\n')
            {
                // break string
                // Add the pending word to the pendingString Builder
                pendingString.Append(pendingWord);

                // Add the finalized string to the strings array
                strings.Add(StringUtils.AlignString(pendingString.ToString().TrimEnd(), Width, Alignment));

                // clear both pendingString and pendingWord
                pendingString.Clear();
                pendingWord.Clear();
            }
            else if (ch == ' ')
            {
                // check if the length of the pending string will be to large if the pending word were to be added
                if (pendingString.Length + pendingWord.Length <= maxWidth)
                {
                    // the string + word length is within de boundary,
                    // add the word to the current string
                    pendingString.Append(pendingWord).Append(' ');

                    // clear the pendingWord
                    pendingWord.Clear();
                }
                else
                {
                    // string will be too large, add the pending to the string array, then clear the pending string
                    strings.Add(StringUtils.AlignString(pendingString.ToString(), Width, Alignment));

                    pendingString.Clear();
                    // put the current word in the cleared pending string with a space.
                    // then clear the pendingWord
                    pendingString.Append(pendingWord).Append(' ');
                    pendingWord.Clear();
                }
            }
            else
            {
                pendingWord.Append(ch);
            }
        }

        // Add the final word to the pending string if the pendingString + pendingWord is within boundary
        if (pendingString.Length + pendingWord.Length < maxWidth)
        {
            pendingString.Append(pendingWord);
            strings.Add(StringUtils.AlignString(pendingString.ToString(), Width, Alignment));
        }
        else
        {
            strings.Add(StringUtils.AlignString(pendingString.ToString(), Width, Alignment));
            strings.Add(StringUtils.AlignString(pendingWord.ToString(), Width, Alignment));
        }

        return strings;
    }
}
