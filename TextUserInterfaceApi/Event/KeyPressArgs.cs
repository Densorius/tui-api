﻿namespace TextUserInterfaceApi.Event;

public class KeyPressedArgs : EventArgs
{
    public ConsoleKeyInfo KeyInfo { get; set; }

    public KeyPressedArgs(ConsoleKeyInfo keyInfo)
    {
        KeyInfo = keyInfo;
    }
}
