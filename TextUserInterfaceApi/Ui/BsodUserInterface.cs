﻿namespace TextUserInterfaceApi.Ui;

class BsodUserInterface : UserInterface
{
    private Label _titleLabel;
    private Label _textLabel;
    private Label _contiueTextLabel;

    private Panel _containerPanel;

    private readonly CharColor _bsodColor;

    public BsodUserInterface(Exception exception)
    {
        string bsodText1 = $"A fatal exception \"{exception.Message}\" has occured at {exception.Source}\n" +
            "The current application will be terminated.\n\n" +
            "* Press any key to terminate the current application\n" +
            "* Press CTRL+ALT+DELETE again to restart your computer.\n" +
            "  You will lose any unsaved information in all applications.";

        string bsodText2 = $"An error has occured. To continue:\n\n" +
            "Press Enter to return to Windows, or\n\n" +
            "Press CTRL+ALT+DELETE again to restart your computer. If you do this,\n" +
            "you will lose any unsaved information in all applications.\n\n" +
            $"Error: \"{exception.Message}\" : {exception.Source}";

        _bsodColor = CharColor.DarkBlue;

        Initialized += (sender, eventArgs) =>
        {
            _textLabel.Text = bsodText1;

            _containerPanel.X = TextUserInterface.Width / 2 - _containerPanel.Width / 2;
            _containerPanel.Y = TextUserInterface.Height / 2 - _containerPanel.Height / 2;

            RootContainer.KeyPressed += (sender2, eventArgs2) =>
            {
                Close();
            };

            TextUserInterface.Resize += Resize;
        };

    }

    private void Resize(object sender, EventArgs eventArgs)
    {
        _containerPanel.X = TextUserInterface.Width / 2 - _containerPanel.Width / 2;
        _containerPanel.Y = TextUserInterface.Height / 2 - _containerPanel.Height / 2;
    }

    protected override void InitializeComponents()
    {
        string titleText = " CMD ";
        string text = "A fatal exception 0E has occured at F0AD:4249C4C\n" +
            "The current application will be terminated.\n\n" +
            "* Press any key to terminate the current application\n" +
            "* Press CTRL+ALT+DELETE again to restart your computer.\n" +
            "  You will lose any unsaved information in all applications.";
        string continueText = "Press any key to continue";

        _containerPanel = new Panel(0, 0, 90, 12) { BGColor = _bsodColor, ShadowOffsetX = 0, ShadowOffsetY = 0 };

        _titleLabel = new Label(titleText, _containerPanel.Width / 2 - titleText.Length / 2, 0, _bsodColor, CharColor.Gray);
        _textLabel = new Label(text, 3, 2) { FGColor = CharColor.Gray };
        _contiueTextLabel = new Label(continueText, _containerPanel.Width / 2 - continueText.Length / 2, 9) { FGColor = CharColor.Gray };

        _containerPanel.AddMultipleElements(_titleLabel, _textLabel, _contiueTextLabel);
        RootContainer.BGColor = _bsodColor;
        RootContainer.AddMultipleElements(_containerPanel);
    }
}
