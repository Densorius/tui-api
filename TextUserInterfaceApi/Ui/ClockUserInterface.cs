﻿using Timers = System.Timers;
using TextUserInterfaceApi.Ui.SnakeGame;

namespace TextUserInterfaceApi.Ui;

public class ClockUserInterface : UserInterface
{
    private Panel _clockPanel;
    private Label _clockLabel;
    private StatusBar _statusBar;
    private MenuBar _menuBar;

    private string _currentTime;

    private readonly Timers.Timer _timer;

    private bool _menuAndStatusBarHidden;

    public ClockUserInterface()
    {
        _timer = new Timers.Timer(500f);
        _menuAndStatusBarHidden = false;
    }

    protected override void InitializeComponents()
    {
        _currentTime = GetTime();

        RootContainer.BGColor = Enum.CharColor.DarkBlue;

        _clockLabel = new Label()
        {
            Text = _currentTime,
            X = 2,
            Y = 1,
            FGColor = Enum.CharColor.Black
        };

        _clockPanel = new Panel()
        {
            Width = _clockLabel.Text.Length + 6,
            Height = 4
        };

        _menuBar = new MenuBar(Enum.CharColor.Black, Enum.CharColor.Gray);

        _statusBar = new StatusBar();
        StatusItem infoStatusItem = new("ESC=Exit F2=Toggle visibility of menubar and statusbar");
        StatusItem copyStatusItem = new("Made by Densorius - 2021");

        Menu fileMenu = new();
        MenuItem exitMenuItem = new("Exit");

        exitMenuItem.Activated += (sender, eventArgs) => StopApplication();

        fileMenu.AddMenuItem(exitMenuItem);

        Menu interfacesMenu = new();
        MenuItem snakeMenuItem = new("Snake");
        MenuItem demoMenuItem = new("Demo");

        snakeMenuItem.Activated += (sender, eventArgs) =>
        {
            StopTimer();
            TextUserInterface.SwitchInterface(new MainMenuInterface(0));
        };

        demoMenuItem.Activated += (sender, eventArgs) =>
        {
            StopTimer();
            TextUserInterface.SwitchInterface(new DemoUserInterface());
        };

        interfacesMenu.AddMultipleMenuItems(snakeMenuItem, demoMenuItem);

        _statusBar.AddMultipleStatusItems(infoStatusItem, copyStatusItem);

        _menuBar.AddMenuBarItem(new MenuBarItem(fileMenu, "File"));
        _menuBar.AddMenuBarItem(new MenuBarItem(interfacesMenu, "Interfaces"));

        _menuBar.ModifiersForActivation = new List<ConsoleModifiers>() { ConsoleModifiers.Alt };
        _menuBar.SecundaryKeyToActivate = ConsoleKey.Applications;

        _clockPanel.AddElement(_clockLabel);

        PanelToCenter();

        RootContainer.AddMultipleElements(_clockPanel, _statusBar, _menuBar);

        RootContainer.KeyPressed += (sender, eventArgs) =>
        {
            if (eventArgs.KeyInfo.Key == ConsoleKey.F2)
            {
                ToggleMenuAndStatusBarVisibilty();
            }
        };

        TextUserInterface.Resize += TextUserInterface_Resize;
        Initialized += ClockUserInterface_Initialized;
    }



    private void TextUserInterface_Resize(object sender, EventArgs e)
    {
        PanelToCenter();
    }

    private void ClockUserInterface_Initialized(object sender, EventArgs e)
    {
        _timer.Elapsed += Timer_Elapsed;
        _timer.Start();
    }

    private void Timer_Elapsed(object sender, Timers.ElapsedEventArgs e)
    {
        TextUserInterface.Update();

        _currentTime = GetTime();
        _clockLabel.Text = _currentTime;
    }

    private void PanelToCenter()
    {
        _clockPanel.X = TextUserInterface.Width / 2 - _clockPanel.InnerWidth / 2;
        _clockPanel.Y = TextUserInterface.Height / 2 - _clockPanel.InnerHeight / 2 - 1;
    }

    private static string GetTime()
    {
        return DateTime.Now.ToString("HH:mm:ss");
    }

    private void StopApplication()
    {
        StopTimer();
        Close();
    }

    private void StopTimer()
    {
        _timer.Stop();
        _timer.Dispose();
    }

    private void ToggleMenuAndStatusBarVisibilty()
    {
        _menuAndStatusBarHidden = !_menuAndStatusBarHidden;

        _menuBar.Hidden = _menuAndStatusBarHidden;
        _statusBar.Hidden = _menuAndStatusBarHidden;
    }
}
