﻿namespace TextUserInterfaceApi.Ui.SnakeGame;

public class Segment
{
    public int X { get; set; }
    public int Y { get; set; }

    public CharColor Color { get; set; }

    public Segment(int x, int y, CharColor color)
    {
        X = x;
        Y = y;
        Color = color;
    }
}
