﻿namespace TextUserInterfaceApi.Ui.SnakeGame;

public enum Direction
{
    UP, LEFT, RIGHT, DOWN
}
