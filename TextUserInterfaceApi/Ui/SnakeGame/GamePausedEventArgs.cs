﻿namespace TextUserInterfaceApi.Ui.SnakeGame;

public class GamePausedEventArgs
{
    public bool GamePaused { get; set; }
}
