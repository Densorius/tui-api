﻿namespace TextUserInterfaceApi.Ui.SnakeGame;

public class MainMenuInterface : UserInterface
{
    public int Score { get; set; }
    public int Highscore { get; set; }

    private Panel _mainMenuPanel;
    private Border _mainMenuBorder;

    private const int PANEL_WIDTH = 80;
    private const int PANEL_HEIGHT = 25;
    private const int PANEL_OFFSET = 1;

    public MainMenuInterface(int highScore)
    {
        Score = 0;
        Highscore = highScore;
    }

    protected override void InitializeComponents()
    {
        TextUserInterface.Title = "Snake";

        _mainMenuPanel = new Panel();
        _mainMenuPanel.Width = PANEL_WIDTH + _mainMenuPanel.ShadowOffsetX;
        _mainMenuPanel.Height = PANEL_HEIGHT + _mainMenuPanel.ShadowOffsetY;
        _mainMenuPanel.X = (Width / 2 - _mainMenuPanel.Width / 2) + PANEL_OFFSET;
        _mainMenuPanel.Y = (Height / 2 - _mainMenuPanel.Height / 2) + PANEL_OFFSET;

        _mainMenuBorder = new Border(0, 0, _mainMenuPanel.InnerWidth, _mainMenuPanel.InnerHeight);
        _mainMenuBorder.CreateFullSizeBorder();

        Label titleLabel = new()
        {
            Text = string.Empty
        };

        titleLabel.Text += "████████  ██      ██      ██      ██    ██  ████████\n";
        titleLabel.Text += "██        ████    ██    ██  ██    ██  ██    ██      \n";
        titleLabel.Text += "████████  ██  ██  ██  ██      ██  ████      ██████  \n";
        titleLabel.Text += "      ██  ██    ████  ██████████  ██  ██    ██      \n";
        titleLabel.Text += "████████  ██      ██  ██      ██  ██    ██  ████████\n";

        titleLabel.X = _mainMenuPanel.InnerWidth / 2 - titleLabel.Width / 2;
        titleLabel.Y = 3;

        Label highScoreLabel = new()
        {
            Text = $"Highscore: {Highscore}",
            Y = 11
        };

        Label scoreLabel = new()
        {
            Text = $"Score: {Score}",
            Y = 12
        };

        highScoreLabel.X = _mainMenuPanel.InnerWidth / 2 - highScoreLabel.Width / 2;
        scoreLabel.X = _mainMenuPanel.InnerWidth / 2 - scoreLabel.Width / 2;

        Menu mainMenu = new(0, 15, 6);

        MenuItem startItem = new("Start");
        MenuItem exitItem = new("Exit");

        startItem.Activated += (sender, args) =>
        {
            TextUserInterface.SwitchInterface(new SnakeGameUserInterface(Highscore));
        };

        exitItem.Activated += (sender, args) =>
        {
            Close();
        };

        mainMenu.AddMultipleMenuItems(startItem, exitItem);

        mainMenu.X = _mainMenuPanel.InnerWidth / 2 - mainMenu.Width / 2;

        Border menuBorder = new(mainMenu.X - 2, mainMenu.Y - 1, mainMenu.Width + 4, mainMenu.Height + 2);
        menuBorder.CreateFullSizeBorder();

        Label copyLabel = new()
        {
            Text = "Recreated by Densorius - 2021",

            Y = _mainMenuBorder.Height - 4,
            Alignment = Enum.TextAlignment.CENTER
        };
        copyLabel.X = _mainMenuPanel.InnerWidth / 2 - copyLabel.Width / 2;

        _mainMenuPanel.AddMultipleElements(_mainMenuBorder, titleLabel, highScoreLabel, scoreLabel, mainMenu, menuBorder, copyLabel);

        RootContainer.AddElement(_mainMenuPanel);

        //TextUserInterface.UpdateOnKeyPress = true;

        TextUserInterface.Resize += OnResize;
    }

    private void OnResize(object sender, EventArgs eventArgs)
    {
        _mainMenuPanel.X = Width / 2 - _mainMenuPanel.Width / 2 + PANEL_OFFSET;
        _mainMenuPanel.Y = Height / 2 - _mainMenuPanel.Height / 2 + PANEL_OFFSET;
    }
}
