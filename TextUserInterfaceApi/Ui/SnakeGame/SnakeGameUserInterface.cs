﻿using Timers = System.Timers;
using TextUserInterfaceApi.Elements.Borders;
using TextUserInterfaceApi.Elements.ButtonStyles;

namespace TextUserInterfaceApi.Ui.SnakeGame
{
	class SnakeGameUserInterface : UserInterface
	{
		private Panel _gamePanel;
		private Border _gameBorder;
		private Canvas _gameCanvas;
		private Label _scoreLabel;
		private Label _highScoreLabel;
		
		private Panel _pausedPanel;

		private Panel _gameOverPanel;

		private int HighScore;
		private int Score;

		private string _scoreStringFormat;
		private string _highScoreStringFormat;

		private Timers.Timer _timer;
		private Game _game;

		private const int PANEL_WIDTH = 80;
		private const int PANEL_HEIGHT = 25;
		private const int PANEL_OFFSET = 1;

		private const int SCORE_LABEL_X = 2;
		private const int HIGHSCORE_LABEL_X = 30;

		private const double TIMER_INTERVAL = 200;

		public SnakeGameUserInterface(int highScore)
		{
			HighScore = highScore;
			Score = 0;

			_scoreStringFormat = "Score: {0}";
			_highScoreStringFormat = "Highscore: {0}";

			_timer = new Timers.Timer(TIMER_INTERVAL);
			_timer.Elapsed += Timer_Elapsed;
		}

		protected override void InitializeComponents()
		{
			TextUserInterface.UpdateOnKeyPress = false;

			SetupMainUI();
			SetupPausedPanel();
			SetupGameOverPanel();

			RootContainer.KeyPressed += (sender, eventArgs) =>
			{
				if (_game != null && !_game.GamePaused)
				{
					_game.KeyInput(eventArgs.KeyInfo.Key);
				} 
				else if (eventArgs.KeyInfo.Key == ConsoleKey.Escape && _game != null && _game.GamePaused && !_pausedPanel.Hidden)
				{
					UnpauseGame();
				}
			};

			Initialized += SnakeGameUserInterface_Initialized;
			TextUserInterface.Resize += TextUserInterface_Resize;
		}

		/// <summary>
		/// Sets tge main Ui up. This includes the panel, border, HUD and the canvas itself
		/// </summary>
		private void SetupMainUI()
		{
			_gamePanel = new Panel();
			_gamePanel.Width = PANEL_WIDTH + _gamePanel.ShadowOffsetX;
			_gamePanel.Height = PANEL_HEIGHT + _gamePanel.ShadowOffsetY;

			_gameBorder = new Border(0, 0, _gamePanel.InnerWidth, _gamePanel.InnerHeight);
			_gameBorder.CreateFullSizeBorder();
			_gameBorder.BorderLines.Add(new BorderLine(0, 2, _gameBorder.Width, Axis.X));

			_gameCanvas = new Canvas(1, 3, _gamePanel.InnerWidth - 2, _gamePanel.InnerHeight - 4)
			{
				BackgroundFillColor = CharColor.DarkRed
			};

			_scoreLabel = new Label(string.Format(_scoreStringFormat, 0), SCORE_LABEL_X, 1);
			_highScoreLabel = new Label(string.Format(_highScoreStringFormat, HighScore), HIGHSCORE_LABEL_X, 1);

			_gamePanel.AddMultipleElements(_gameBorder, _scoreLabel, _highScoreLabel, _gameCanvas);

			CenterPanel();

			RootContainer.AddElement(_gamePanel);
		}

		/// <summary>
		/// Sets up the pause panel. This includes a menu so the user can either continue or exit the game
		/// </summary>
		private void SetupPausedPanel()
		{
			_pausedPanel = new() { Width = 30, Height = 10, BGColor = CharColor.White };
			_pausedPanel.X = _gamePanel.Width / 2 - _pausedPanel.Width / 2;
			_pausedPanel.Y = _gamePanel.Height / 2 - _pausedPanel.Height / 2;

			Border pausedPanelBorder = new(0, 0, _pausedPanel.InnerWidth, _pausedPanel.InnerHeight);
			pausedPanelBorder.CreateFullSizeBorder();

			Label pausedLabel = new() { Text = "PAUSED", Y = 2 };
			pausedLabel.X = (_pausedPanel.Width / 2 - pausedLabel.Text.Length / 2) - 1;

			Menu pauseMenu = new() { BGColor = CharColor.White, HighlightFGColor = CharColor.White };
			MenuItem continueMenuItem = new("Continue");
			MenuItem exitMenuItem = new("Exit");

			continueMenuItem.Activated += ContinueMenuItem_Activated;
			exitMenuItem.Activated += ExitMenuItem_Activated;

			pauseMenu.AddMenuItems(continueMenuItem, exitMenuItem);

			pauseMenu.X = _pausedPanel.Width / 2 - pauseMenu.Width / 2 - 1;
			pauseMenu.Y = _pausedPanel.Height / 2 - pauseMenu.Height / 2 + 1;

			Border pauseMenuBorder = new(pauseMenu.X - 1, pauseMenu.Y - 1, pauseMenu.Width + 2, pauseMenu.Height + 2);
			pauseMenuBorder.CreateFullSizeBorder();

			_pausedPanel.AddMultipleElements(pausedPanelBorder, pausedLabel, pauseMenu, pauseMenuBorder);

			_pausedPanel.Hidden = true;

			_gamePanel.AddElement(_pausedPanel);
		}

		/// <summary>
		/// Sets up the game over panel. This includes a button the user can "press" to exit the game.
		/// </summary>
		private void SetupGameOverPanel()
		{
			_gameOverPanel = new Panel() { Width = 30, Height = 8, BGColor = CharColor.White };

			_gameOverPanel.X = _gamePanel.Width / 2 - _gameOverPanel.Width / 2;
			_gameOverPanel.Y = _gamePanel.Height / 2 - _gameOverPanel.Height / 2;

			Border border = new(0, 0, _gameOverPanel.InnerWidth, _gameOverPanel.InnerHeight);
			border.CreateFullSizeBorder();

			Label gameOverLabel = new() { Text = "GAME OVER", Y = 2 };
			gameOverLabel.X = _gameOverPanel.InnerWidth / 2 - gameOverLabel.Width / 2;

			Button exitButton = new() { Width = 9, Text = "Exit", Focused = true };
			ButtonArrowStyle style = new() { BackgroundColor = CharColor.Gray };

			exitButton.ButtonStyle = style;
			
			exitButton.X = _gameOverPanel.X / 2 - exitButton.Width / 2 + 1;
			exitButton.Y = _gameOverPanel.InnerHeight - 3;

			exitButton.Clicked += ExitButton_Clicked;

			_gameOverPanel.AddMultipleElements(border, gameOverLabel, exitButton);

			_gameOverPanel.Hidden = true;

			_gamePanel.AddElement(_gameOverPanel);
		}

		private void ExitButton_Clicked(object sender, Event.KeyPressedArgs e)
		{
			if (!_gameOverPanel.Hidden)
			{
				ExitGame();
			}
		}

		private void ExitMenuItem_Activated(object sender, EventArgs e)
		{
			if (!_pausedPanel.Hidden)
			{
				ExitGame();
			}
		}

		private void ContinueMenuItem_Activated(object sender, EventArgs e)
		{
			UnpauseGame();
		}

		private void SnakeGameUserInterface_Initialized(object sender, EventArgs e)
		{
			_game = new Game(_gameCanvas.Width, _gameCanvas.Height, HighScore);

			_game.ScoreIncreased += (sender, scoreEventArgs) =>
			{
				_scoreLabel.Text = string.Format(_scoreStringFormat, scoreEventArgs.Score);
			};

			_game.GamePausedEvt += (sender, gamePausedEventArgs) =>
			{
				if (gamePausedEventArgs.GamePaused)
				{
					Menu menu = _pausedPanel.ChildElements[2] as Menu;

					menu.MenuItems.SetIndexToFirst();

					_pausedPanel.Hidden = false;

					// update the ui to actually show the panel and also set updateOnKeyPress to true so the menu will update normally.
					TextUserInterface.Update();
					TextUserInterface.UpdateOnKeyPress = true;
				}
			};

			_game.GameOverEvt += (sender, eventArgs) =>
			{
				_gameOverPanel.Hidden = false;

				TextUserInterface.Update();
				TextUserInterface.UpdateOnKeyPress = true;
			};

			_timer.Start();
		}

		private void Timer_Elapsed(object sender, Timers.ElapsedEventArgs e)
		{
			if (!_game.GamePaused)
				TextUserInterface.Update();

			_game.Update(_gameCanvas);
		}

		private void CenterPanel()
		{
			_gamePanel.X = TextUserInterface.Width / 2 - _gamePanel.Width / 2 + PANEL_OFFSET;
			_gamePanel.Y = TextUserInterface.Height / 2 - _gamePanel.Height / 2 + PANEL_OFFSET;
		}

		private void TextUserInterface_Resize(object sender, EventArgs e)
		{
			CenterPanel();
		}

		private void UnpauseGame()
		{
			_pausedPanel.Hidden = true;
			TextUserInterface.UpdateOnKeyPress = false;
			TextUserInterface.Update();
			_game.Unpause();
		}
		
		private void ExitGame()
		{
			_timer.Dispose();

			TextUserInterface.UpdateOnKeyPress = true;

			// return to the main menu.
			TextUserInterface.SwitchInterface(new MainMenuInterface(HighScore));
		}
	}
}
