﻿namespace TextUserInterfaceApi.Ui.SnakeGame;

public class Snake
{
    public Queue<Segment> Segments { get; private set; }

    public int HeadX { get; private set; }
    public int HeadY { get; private set; }

    public Direction Direction { get; set; }

    public CharColor HeadColor;
    public CharColor SegmentColor;

    public Snake(int x, int y)
    {
        Segments = new Queue<Segment>();

        HeadX = x;
        HeadY = y;

        Direction = Direction.UP;
        HeadColor = CharColor.Green;
        SegmentColor = CharColor.DarkGreen;

        // create initial segments for the snake.
        Segments.Enqueue(new Segment(HeadX, HeadY + 1, SegmentColor));
        Segments.Enqueue(new Segment(HeadX, HeadY + 1, SegmentColor));
        Segments.Enqueue(new Segment(HeadX, HeadY, HeadColor));
    }

    public void Draw(Canvas canvas)
    {
        foreach (Segment segment in Segments)
        {
            canvas.BackgroundFillColor = segment.Color;
            canvas.FillCharacter = ' ';
            canvas.DrawRectangle(segment.X * 2, segment.Y, 2, 1);
        }
    }

    public void Move()
    {
        switch (Direction)
        {
            case Direction.UP: HeadY--; break;
            case Direction.RIGHT: HeadX++; break;
            case Direction.DOWN: HeadY++; break;
            case Direction.LEFT: HeadX--; break;
        }

        UpdateSnakeSegments(false);
    }

    public void Grow()
    {
        UpdateSnakeSegments(true);
    }

    private void UpdateSnakeSegments(bool grow)
    {
        if (!grow)
        {
            Segments.Dequeue();
        }

        foreach (Segment segment in Segments)
        {
            segment.Color = SegmentColor;
        }

        Segments.Enqueue(new Segment(HeadX, HeadY, HeadColor));
    }
}
