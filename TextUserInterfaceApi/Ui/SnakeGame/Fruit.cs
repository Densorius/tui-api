﻿namespace TextUserInterfaceApi.Ui.SnakeGame;

public class Fruit
{
    public int X { get; set; }
    public int Y { get; set; }

    public int Width { get; init; }
    public int Height { get; init; }

    public CharColor Color { get; set; }

    public Fruit(int width = 2, int height = 1)
    {
        Width = width;
        Height = height;

        Color = CharColor.DarkRed;
    }

    public void Draw(Canvas canvas)
    {
        canvas.BackgroundFillColor = Color;
        canvas.DrawRectangle(X * 2, Y, Width, Height);
    }
}
