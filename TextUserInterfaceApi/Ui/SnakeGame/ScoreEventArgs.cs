﻿namespace TextUserInterfaceApi.Ui.SnakeGame;

public class ScoreEventArgs : EventArgs
{
    public int Score { get; set; }

    public ScoreEventArgs(int score = 0)
    {
        Score = score;
    }
}
