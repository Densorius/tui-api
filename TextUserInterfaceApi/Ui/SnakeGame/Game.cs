﻿namespace TextUserInterfaceApi.Ui.SnakeGame;

public class Game
{
    public event EventHandler<ScoreEventArgs> ScoreIncreased;
    public event EventHandler<GamePausedEventArgs> GamePausedEvt;
    public event EventHandler<EventArgs> GameOverEvt;

    public int PlayfieldWidth { get; set; }
    public int PlayfieldHeight { get; set; }

    public int Score { get; set; }
    public int HighScore { get; set; }

    public bool GamePaused { get; set; }
    public bool GameOver { get; private set; }

    private readonly Snake _snake;
    private readonly Fruit _fruit;
    private readonly Random _random;

    private const int SCORE_INCREMENT_VALUE = 10;

    public Game(int width, int height, int highScore)
    {
        PlayfieldWidth = width;
        PlayfieldHeight = height;
        HighScore = highScore;

        Score = 0;

        _snake = new Snake((width / 2) / 2, height / 2); // - 1 to put the initial place of the grid
        _fruit = new Fruit();
        _random = new Random();

        SetRandomFruitLocation();
    }

    public void KeyInput(ConsoleKey consoleKey)
    {
        // change the direction of the snake based on the direction of the pressed arrow key.
        // Direction will not change if it's the oppesite.
        switch (consoleKey)
        {
            case ConsoleKey.UpArrow:
                if (_snake.Direction != Direction.DOWN)
                {
                    _snake.Direction = Direction.UP;
                }
                break;
            case ConsoleKey.RightArrow:
                if (_snake.Direction != Direction.LEFT)
                {
                    _snake.Direction = Direction.RIGHT;
                }
                break;
            case ConsoleKey.DownArrow:
                if (_snake.Direction != Direction.UP)
                {
                    _snake.Direction = Direction.DOWN;
                }
                break;

            case ConsoleKey.LeftArrow:
                if (_snake.Direction != Direction.RIGHT)
                {
                    _snake.Direction = Direction.LEFT;
                }
                break;

            case ConsoleKey.Escape:
                if (!GamePaused && !GameOver)
                {
                    PauseGame();
                }
                break;
        }
    }

    public void Update(Canvas canvas)
    {
        if (!GamePaused && !GameOver)
            _snake.Move();

        canvas.Clear();
        _fruit.Draw(canvas);
        _snake.Draw(canvas);

        if (!GameOver)
        {
            Segment head = _snake.Segments.Last();

            // check if snake hit itself.
            foreach (Segment segment in _snake.Segments)
            {
                if (segment != head && _snake.HeadX == segment.X && _snake.HeadY == segment.Y)
                {
                    SetGameOver();
                }
            }

            // check if snake head is outside of playfield (hit the border)
            if (_snake.HeadX < 0 || _snake.HeadX > PlayfieldWidth / 2 || _snake.HeadY < 0 || _snake.HeadY > PlayfieldHeight)
            {
                SetGameOver();
            }

            if (!GameOver && _snake.HeadX == _fruit.X && _snake.HeadY == _fruit.Y)
            {
                _snake.Grow();

                IncrementScore();
                SetRandomFruitLocation();
            }
        }
    }

    public void Unpause()
    {
        GamePaused = false;
    }

    private void SetRandomFruitLocation()
    {
        int x = 0;
        int y = 0;

        bool noGoodLocation = true;

        // set fruit to random location.
        // If the fruit happens to be on the same space as a segment,
        // try another location until it's not on a segment
        while (noGoodLocation)
        {
            x = _random.Next(0, PlayfieldWidth / 2);
            y = _random.Next(0, PlayfieldHeight);

            foreach (Segment segment in _snake.Segments)
            {
                if (segment.X != x || segment.Y != y)
                {
                    noGoodLocation = false;
                }
                else
                {
                    noGoodLocation = true;
                    break;
                }
            }
        }

        _fruit.X = x;
        _fruit.Y = y;
    }

    private void IncrementScore()
    {
        Score += SCORE_INCREMENT_VALUE;

        ScoreEventArgs scoreEventArgs = new(Score);

        ScoreIncreased?.Invoke(this, scoreEventArgs);
    }

    private void PauseGame()
    {
        GamePaused = true;
        GamePausedEvt?.Invoke(this, new GamePausedEventArgs() { GamePaused = true });
    }

    private void SetGameOver()
    {
        GameOver = true;
        GameOverEvt?.Invoke(this, new EventArgs());
    }
}
