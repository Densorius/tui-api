﻿namespace TextUserInterfaceApi.Ui;

class ShowUserInterface : UserInterface
{
    protected override void InitializeComponents()
    {
        Container container = new(0, 0, Width, 1) { BGColor = Enum.CharColor.Gray };
        Label label = new(" File ", 2, 0) { BGColor = Enum.CharColor.Black, FGColor = Enum.CharColor.Gray };

        Panel panel = new(2, 1, 15, 6);
        Border border = new(0, 0, panel.InnerWidth, panel.InnerHeight);

        Menu menu = new(1, 1, 11);
        menu.AddMenuItem(new MenuItem("Info"));
        menu.AddMenuItem(null);
        menu.AddMenuItem(new MenuItem("Exit"));

        border.CreateFullSizeBorder();
        border.BorderLines.Add(new Elements.Borders.BorderLine(border.X, 2, border.Width, Enum.Axis.X));

        panel.AddMultipleElements(menu, border);
        RootContainer.AddMultipleElements(panel, container, label);
    }
}
