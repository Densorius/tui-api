﻿using Timers = System.Timers;
using TextUserInterfaceApi.Elements.ButtonStyles;
using TextUserInterfaceApi.Event;
using TextUserInterfaceApi.Ui.SnakeGame;

namespace TextUserInterfaceApi.Ui;

class DemoUserInterface : UserInterface
{
    private Panel _panel;

    private Label _countLabel;
    private Label _countLabelHeader;
    private Label _clockLabel;
    private Label _clockLabelHeader;
    private Label _headerLabel;
    private Label _menuLabel;
    private Label _buttonPressedLabel;

    private Border _menuBorder;
    private Border _countLabelBorder;
    private Border _panelBorder;
    private Border _clockBorder;

    private Menu _colorMenu;

    private MenuBar _menuBar;

    private StatusBar _statusBar;
    private readonly Timers.Timer _timer;

    private int _count;
    private int _buttonPressedCount;

    public DemoUserInterface()
    {
        _count = 0;
        _timer = new Timers.Timer(500f);
    }

    protected override void InitializeComponents()
    {
        int panelWidth = 60;
        int panelHeight = 18;
        int panelX = 2;
        int panelY = 2;

        Button button = new() { Text = "Click me!", X = 2, Y = 3, FGColor = CharColor.Black, BGColor = CharColor.White, Focused = true };

        ButtonArrowStyle style = new()
        {
            ArrowCharLeft = ')',
            ArrowCharRight = '(',
            BackgroundColor = CharColor.White,
            ForegroundColor = CharColor.Red
        };

        //button.ButtonStyle = style;

        button.Width = 14;

        _buttonPressedLabel = new Label()
        {
            X = 2,
            Y = 5,
            Text = "Button pressed count: 0",
            FGColor = CharColor.Gray,
            BGColor = CharColor.Transparant
        };

        button.Clicked += (sender, keyPressedArgs) =>
        {
            _buttonPressedLabel.Text = $"Button pressed count: {++_buttonPressedCount}";
        };

        _panel = new Panel(panelX, panelY, panelWidth, panelHeight);
        _panelBorder = new Border(0, 0, _panel.InnerWidth, _panel.InnerHeight);

        _headerLabel = new Label("Text-user-interface api demo.\nMade by Dennis Angevaare (2021)", 2, 2)
        {
            Alignment = TextAlignment.CENTER,
            Width = _panel.InnerWidth - 4
        };

        // menu
        _menuBorder = new Border(2, 5, 26, 11);
        _menuLabel = new Label("Select background color", 3, 5);
        _colorMenu = new Menu(3, 6, 24);
        FillColorMenu(_colorMenu);

        // counter

        _countLabelBorder = new Border(30, 5, 26, 5);
        _countLabelHeader = new Label("Keypress counter", 31, 5);
        _countLabel = new Label($"Count: {_count}", _countLabelBorder.X + _countLabelBorder.Width / 2 - $"Count: {_count}".Length / 2, 7);

        // clock
        string time = GetTime();

        _clockBorder = new Border(30, 11, 26, 5);
        _clockLabelHeader = new Label("Clock", 31, 11);
        _clockLabel = new Label(time, _clockBorder.X + (_clockBorder.Width / 2 - time.Length / 2), 13);

        _panel.AddMultipleElements(_headerLabel, _countLabel, _countLabelBorder,
        _countLabelHeader, _panelBorder, _clockBorder, _clockLabelHeader, _clockLabel, _menuBorder, _menuLabel, _colorMenu);

        _clockBorder.CreateFullSizeBorder();
        _countLabelBorder.CreateFullSizeBorder();
        _menuBorder.CreateFullSizeBorder();
        _panelBorder.CreateFullSizeBorder();

        _statusBar = new StatusBar();
        _statusBar.AddStatusItem(new StatusItem("ESC=Exit  BACKSPACE=Reset counter  ANY KEY=Increase counter  SHIFT+KEY=Double increase"));
        _statusBar.AddStatusItem(new StatusItem("Made by D. Angevaare (2021) "));


        Menu fileMenu = new();
        MenuItem exitMenuItem = new("Exit");

        exitMenuItem.Activated += (sender, eventArgs) => StopApplication();

        fileMenu.AddMultipleMenuItems(exitMenuItem);

        Menu interfacesMenu = new();
        MenuItem snakeMenuItem = new("Snake");
        MenuItem clockMenuItem = new("Clock");

        snakeMenuItem.Activated += (sender, eventArgs) =>
        {
            StopTimer();
            TextUserInterface.SwitchInterface(new MainMenuInterface(0));
        };

        clockMenuItem.Activated += (sender, eventArgs) =>
        {
            StopTimer();
            TextUserInterface.SwitchInterface(new ClockUserInterface());
        };

        interfacesMenu.AddMultipleMenuItems(snakeMenuItem, clockMenuItem);

        Menu help = new();
        help.AddMenuItem(new MenuItem("How to use"));
        help.AddMenuItem(null);
        help.AddMenuItem(new MenuItem("About"));

        _menuBar = new MenuBar(CharColor.Black, CharColor.Gray);
        _menuBar.AddMenuBarItem(new MenuBarItem(fileMenu, "File"));
        _menuBar.AddMenuBarItem(new MenuBarItem(interfacesMenu, "Interfaces"));
        _menuBar.AddMenuBarItem(new MenuBarItem(help, "Help"));

        _menuBar.ModifiersForActivation = new List<ConsoleModifiers>() { ConsoleModifiers.Alt };
        _menuBar.SecundaryKeyToActivate = ConsoleKey.Applications;

        RootContainer.AddMultipleElements(button, _buttonPressedLabel, _panel, _statusBar, _menuBar);
        RootContainer.KeyPressed += RootContainer_KeyPressed;


        TextUserInterface.Resize += TextUserInterface_Resize;

        Initialized += DemoUserInterface_Initialized;

        _panel.X = TextUserInterface.Width / 2 - _panel.Width / 2;
        _panel.Y = TextUserInterface.Height / 2 - _panel.Height / 2;
    }

    private void TextUserInterface_Resize(object sender, EventArgs e)
    {
        _panel.X = Width / 2 - _panel.Width / 2;
        _panel.Y = Height / 2 - _panel.Height / 2;
    }

    private void DemoUserInterface_Initialized(object sender, EventArgs e)
    {
        //TextUserInterface.ConsoleCursorHelper.CursorVisible = true;
        //Console.CursorVisible = true;

        _timer.Elapsed += Timer_Elapsed;
        _timer.Start();
    }

    private void Timer_Elapsed(object sender, Timers.ElapsedEventArgs e)
    {
        TextUserInterface.Update();

        string time = GetTime();
        _clockLabel.Text = time;
        _clockLabel.X = _clockBorder.X + (_clockBorder.Width / 2 - time.Length / 2);
    }

    private void RootContainer_KeyPressed(object sender, KeyPressedArgs keyPressedArgs)
    {
        if (!_menuBar.Focus)
        {
            switch (keyPressedArgs.KeyInfo.Key)
            {
                case ConsoleKey.Escape:
                    if (!_menuBar.Focus)
                    {
                        StopApplication();
                    }
                    break;
                case ConsoleKey.Backspace:
                    _count = 0;
                    break;
                default:
                    if (keyPressedArgs.KeyInfo.Modifiers.HasFlag(ConsoleModifiers.Shift))
                        _count += 2;
                    else
                        _count++;
                    break;
            }
        }

        _countLabel.Text = $"Count: {_count}";
        _countLabel.X = _countLabelBorder.X + _countLabelBorder.Width / 2 - $"Count: {_count}".Length / 2;
    }

    /// <summary>
    /// Fills a given menu.
    /// </summary>
    /// <param name="menu">Menu to fill.</param>
    private void FillColorMenu(Menu menu)
    {
        MenuItem darkBlueMenuItem = new("Dark blue");
        MenuItem darkCyanMenuItem = new("Dark cyan");
        MenuItem darkRedMenuitem = new("Dark red");
        MenuItem darkYellowMenuItem = new("Dark yellow");
        MenuItem darkGreenMenuItem = new("Dark green");
        MenuItem darkMagentaMenuItem = new("Dark magenta");
        MenuItem darkGrayMenuItem = new("Dark gray");
        MenuItem grayMenuItem = new("Gray");
        MenuItem whiteMenuItem = new("White");

        darkBlueMenuItem.Activated += (sender, eventArgs) => RootContainer.BGColor = CharColor.DarkBlue;
        darkCyanMenuItem.Activated += (sender, eventArgs) => RootContainer.BGColor = CharColor.DarkCyan;
        darkRedMenuitem.Activated += (sender, eventArgs) => RootContainer.BGColor = CharColor.DarkRed;
        darkYellowMenuItem.Activated += (sender, eventArgs) => RootContainer.BGColor = CharColor.DarkYellow;
        darkGreenMenuItem.Activated += (sender, eventArgs) => RootContainer.BGColor = CharColor.DarkGreen;
        darkMagentaMenuItem.Activated += (sender, eventArgs) => RootContainer.BGColor = CharColor.DarkMagenta;
        darkGrayMenuItem.Activated += (sender, eventArgs) => RootContainer.BGColor = CharColor.DarkGray;
        grayMenuItem.Activated += (sender, eventArgs) => RootContainer.BGColor = CharColor.Gray;
        whiteMenuItem.Activated += (sender, eventArgs) => RootContainer.BGColor = CharColor.White;

        menu.AddMultipleMenuItems(darkBlueMenuItem,
            darkCyanMenuItem,
            darkRedMenuitem,
            darkYellowMenuItem,
            darkGreenMenuItem,
            darkMagentaMenuItem,
            darkGrayMenuItem,
            grayMenuItem,
            whiteMenuItem);
    }

    private static string GetTime()
    {
        return DateTime.Now.ToString("HH:mm:ss");
    }

    private void StopApplication()
    {
        StopTimer();
        Close();
    }

    private void StopTimer()
    {
        _timer.Stop();
        _timer.Dispose();
    }
}
